---
title: "It's Time For a New Text Editor"
date: 2020-03-20T17:40:00+01:00
image: images/blog/0013.png
author: Klaus-Dieter Schmatz
---

Let's be honest.
Vim is bloated.
Sure, it's a great, feature-rich editor with an impressive ecosystem of plugins.
Unfortunately, not only that.
Vim is also a mediocre file and directory browser, a lacklustre spell checker, a poor window manager, and other things.
Sure, Vim is way more focused on editing text than Emacs, but that's true for any editor.

### sudo pacman -Rs vim

During its 28 years of active development, Vim has accumulated a lot of technical debt and redundant functionality that should better have been implemented separately.
The verdict: Vim is guilty of violating one of the cornerstones of the UNIX philosophy: Write programs that do one thing and do it well.
The sentence: `sudo pacman -Rs vim`.

In fact, I gave up Vim in favor of Neovim quite some time ago.
The Neovim project maintains compatibility with Vim while trying to improve the codebase.
Indeed, instead of Vim's 300K lines of code, Neovim weighs in at comparatively modest 175K.
Still huge though, and with tons of irrelevant features.

### What's the Alternative?

Turn to [Stuff that rocks](https://suckless.org/rocks/) on suckless.org:

	vis - A modern, legacy free, simple yet efficient vim-like editor.

Vis is not compatible with Vim by design, but it uses the same keybindings for the most part.
It does not come with the same feature set and you can't use plugins written in Vimscript.
Extensions can be implemented in Lua, arguably a much better language than Vimscript.
And there are cool _text editing features_ such as structural regular expressions (which can span lines) and multiple cursors/selections, which are not available in Vim.

### Get Started

After installing Vis with your favorite package manager, set up your init script in `~/.config/vis/visrc.lua`.
I like to keep things simple, so mine is quite short:

	require('vis')
	require('plugins/vis-modelines')
	require('plugins/vis-commentary')
	require('plugins/vis-filetype-settings')

	settings = {
		java = { 'set autoindent', 'set expandtab', 'set tabwidth 4' },
		yaml = { 'set autoindent', 'set expandtab', 'set tabwidth 2' }
	}

	vis.events.subscribe(vis.events.INIT, function()
		vis:command('set theme gruvbox')
	end)

	vis.events.subscribe(vis.events.WIN_OPEN, function(win)
		vis:command('set relativenumbers')
		vis:command('set cursorline')
	end)

Plugins are available [here](https://github.com/martanne/vis/wiki/Plugins) and themes [here](https://github.com/martanne/vis/wiki/Themes).
My plugins and themes are installed in `~/.config/vis`:

	  vis
	├──   plugins
	│  ├──   vis-commentary.lua
	│  ├──   vis-filetype-settings.lua
	│  └──   vis-modelines.lua
	├──   themes
	│  └──   gruvbox.lua
	└──   visrc.lua

### Not For Everybody

If you really like _netrw_ or rely on dozens of Vim plugins, Vis is not for you.
If on the other hand all you want is a simple yet efficient editor with Vim keybindings, you may appreciate Vis.
