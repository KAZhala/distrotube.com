---
title: "Kubuntu 18.04 'Bionic Beaver' Install and Review"
image: images/thumbs/0190.jpg
date: Sat, 28 Apr 2018 22:51:12 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Kubuntu"]
---

#### VIDEO

{{< amazon src="Kubuntu+18.04+Bionic+Beaver+Install+and+Review.mp4" >}}
&nbsp;

#### SHOW NOTES

It's release week for the various flavors of Ubuntu 18.04. Today, I'm looking at Kubuntu 18.04 "Bionic Beaver". Kubuntu 18.04 sports the Plasma desktop environment and offers support for up to three years. https://kubuntu.org/
