---
title: "Namib GNU/Linux First Impression Install and Review"
image: images/thumbs/0183.jpg
date: Sun, 22 Apr 2018 22:42:44 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Namib"]
---

#### VIDEO

{{< amazon src="Namib+GNULinux+First+Impression+Install+and+Review.mp4" >}}
&nbsp;

#### SHOW NOTES

In this video, I do a quick install and overview of Namib GNU/Linux. Namib is an Arch-based Linux distro that offers four different dekstop versions. Today, I look at their MATE edition. <a href="https://namib.meerkat.tk/">https://namib.meerkat.tk/</a>
