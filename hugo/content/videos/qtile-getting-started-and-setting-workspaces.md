---
title: "Qtile - Getting Started And Setting Workspaces"
image: images/thumbs/0478.jpg
date: Sun, 15 Dec 2019 00:20:00 +0000
author: Derek Taylor
tags: ["tiling window managers", "qtile"]
---

#### VIDEO

{{< amazon src="Qtile+Getting+Started+And+Setting+Workspaces.mp4" >}}
&nbsp;

#### SHOW NOTES

Are you wanting to try out the Qtile tiling window manager but you are not sure how to configure it to your liking?  In this video, I take you through getting started hacking on the default qtile config and changing the workspace names.
	
#### REFERENCED:
+ ► http://www.qtile.org/ - Qtile Documentation
+ ► https://gitlab.com/dwt1/dotfiles/blob/master/.config/qtile/default_config.py - The Default Config
+ ► https://gitlab.com/dwt1/dotfiles/blob/master/.config/qtile/config.py - My Heavily Modified Config
