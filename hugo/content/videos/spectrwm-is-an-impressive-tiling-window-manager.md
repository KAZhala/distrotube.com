---
title: "Spectrwm Is An Impressive Tiling Window Manager"
image: images/thumbs/0596.jpg
date: 2020-04-14T12:23:40+06:00
author: Derek Taylor
tags: ["spectrwm", "Tiling Window Managers"]
---

#### VIDEO

{{< amazon src="Spectrwm+Is+An+Impressive+Tiling+Window+Manager.mp4" >}}
&nbsp;

#### SHOW NOTES

Spectrwm is a small dynamic tiling and reparenting window manager for X11. It has sane defaults and does not require one to learn a language to do any configuration. It is written by hackers for hackers and it strives to be small, compact and fast.  It was largely inspired by xmonad and dwm. 

REFERENCED:
+ https://github.com/conformal/spectrwm