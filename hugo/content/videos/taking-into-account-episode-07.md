---
title: "Taking Into Account, Ep. 7 - Linus on Intel, NSA's Speck, NVIDIA RTX, Zero Phone, Releases, Peertube"
image: images/thumbs/0273.jpg
date: Thu, 06 Sep 2018 18:27:48 +0000
author: Derek Taylor
tags: ["Taking Into Account", ""]
---

#### VIDEO

{{< amazon src="Taking+Into+Account%2C+Ep.+7+-+Linus+on+Intel%2C+NSAs+Speck%2C+NVIDIA+RTX%2C+Zero+Phone%2C+Releases%2C+Peertube.mp4" >}}
&nbsp;

#### SHOW NOTES

This weeks topics include: 

<a href="https://www.youtube.com/watch?v=B-TX5-YYhBQ&amp;t=48s">0:48 Linus Torvalds talks about lessons learned from Intel's security bugs. 

<a href="https://www.youtube.com/watch?v=B-TX5-YYhBQ&amp;t=509s">8:29 The NSA Speck algorithm will be removed in Linux kernel 4.20 

<a href="https://www.youtube.com/watch?v=B-TX5-YYhBQ&amp;t=784s">13:04 Reasons why Linux gamers might want to pass on NVIDIA RTX series of GPUs. 

<a href="https://www.youtube.com/watch?v=B-TX5-YYhBQ&amp;t=1153s">19:13 Coming soon - the Zero Phone 

<a href="https://www.youtube.com/watch?v=B-TX5-YYhBQ&amp;t=1457s">24:17 New releases: Mastodon 2.5, Firefox 62.0 and Tails 3.9 

<a href="https://www.youtube.com/watch?v=B-TX5-YYhBQ&amp;t=1850s">30:50 I read a view question 

REFERENCED IN THE VIDEO: 

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=B-TX5-YYhBQ&amp;redir_token=WarM2CIDz16UGEmQFKzLy4g9iw58MTU1MzQ1MjA4OUAxNTUzMzY1Njg5&amp;event=video_description&amp;q=https%3A%2F%2Fwww.zdnet.com%2Farticle%2Flinus-torvalds-talks-frankly-about-intel-security-bugs%2F" target="_blank">https://www.zdnet.com/article/linus-t...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=B-TX5-YYhBQ&amp;redir_token=WarM2CIDz16UGEmQFKzLy4g9iw58MTU1MzQ1MjA4OUAxNTUzMzY1Njg5&amp;event=video_description&amp;q=https%3A%2F%2Fwww.tomshardware.com%2Fnews%2Fnsa-speck-removed-linux-4-20%2C37747.html" target="_blank">https://www.tomshardware.com/news/nsa..

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=B-TX5-YYhBQ&amp;redir_token=WarM2CIDz16UGEmQFKzLy4g9iw58MTU1MzQ1MjA4OUAxNTUzMzY1Njg5&amp;event=video_description&amp;q=https%3A%2F%2Fwww.tomshardware.com%2Fnews%2Fnsa-speck-removed-linux-4-20%2C37747.html" target="_blank">.<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=B-TX5-YYhBQ&amp;redir_token=WarM2CIDz16UGEmQFKzLy4g9iw58MTU1MzQ1MjA4OUAxNTUzMzY1Njg5&amp;event=video_description&amp;q=https%3A%2F%2Fwww.phoronix.com%2Fscan.php%3Fpage%3Dnews_item%26px%3D10-Reasons-Pass-RTX-20-Linux" target="_blank">https://www.phoronix.com/scan.php?pag...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=B-TX5-YYhBQ&amp;redir_token=WarM2CIDz16UGEmQFKzLy4g9iw58MTU1MzQ1MjA4OUAxNTUzMzY1Njg5&amp;event=video_description&amp;q=https%3A%2F%2Ffossbytes.com%2Fzerophone-raspberry-pi-open-source-linux-crowdsupply%2F" target="_blank">https://fossbytes.com/zerophone-raspb...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=B-TX5-YYhBQ&amp;redir_token=WarM2CIDz16UGEmQFKzLy4g9iw58MTU1MzQ1MjA4OUAxNTUzMzY1Njg5&amp;event=video_description&amp;q=https%3A%2F%2Fwww.crowdsupply.com%2Farsenijs%2Fzerophone" target="_blank">https://www.crowdsupply.com/arsenijs/..

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=B-TX5-YYhBQ&amp;redir_token=WarM2CIDz16UGEmQFKzLy4g9iw58MTU1MzQ1MjA4OUAxNTUzMzY1Njg5&amp;event=video_description&amp;q=https%3A%2F%2Fwww.crowdsupply.com%2Farsenijs%2Fzerophone" target="_blank">.<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=B-TX5-YYhBQ&amp;redir_token=WarM2CIDz16UGEmQFKzLy4g9iw58MTU1MzQ1MjA4OUAxNTUzMzY1Njg5&amp;event=video_description&amp;q=https%3A%2F%2Fblog.joinmastodon.org%2F2018%2F09%2Fmastodon-2.5-released%2F" target="_blank">https://blog.joinmastodon.org/2018/09...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=B-TX5-YYhBQ&amp;redir_token=WarM2CIDz16UGEmQFKzLy4g9iw58MTU1MzQ1MjA4OUAxNTUzMzY1Njg5&amp;event=video_description&amp;q=https%3A%2F%2Fwww.mozilla.org%2Fen-US%2Ffirefox%2F62.0%2Freleasenotes%2F" target="_blank">https://www.mozilla.org/en-US/firefox...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=B-TX5-YYhBQ&amp;redir_token=WarM2CIDz16UGEmQFKzLy4g9iw58MTU1MzQ1MjA4OUAxNTUzMzY1Njg5&amp;event=video_description&amp;q=https%3A%2F%2Ftails.boum.org%2Fnews%2Fversion_3.9%2Findex.en.html" target="_blank">https://tails.boum.org/news/version_3...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=B-TX5-YYhBQ&amp;redir_token=WarM2CIDz16UGEmQFKzLy4g9iw58MTU1MzQ1MjA4OUAxNTUzMzY1Njg5&amp;event=video_description&amp;q=https%3A%2F%2Fjoinpeertube.org%2Fen%2Fhome%2F" target="_blank">https://joinpeertube.org/en/home/
