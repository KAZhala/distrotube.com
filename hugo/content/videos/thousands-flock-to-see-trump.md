---
title: "Thousands Flock To See Trump"
image: images/thumbs/0460.jpg
date: Wed, 06 Nov 2019 00:13:00 +0000
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

{{< amazon src="Thousands+Flock+To+See+Trump.mp4" >}}
&nbsp;

#### SHOW NOTES

This is an off-topic video of some footage I shot at a Donald Trump rally that was held in my hometown earlier today. Thousands showed up and waited in line for many, many hours with the hope of actually getting inside to see the President speak. I waited in line almost six hours. They were only allowing 6,000 inside the venue and the rest were going to have to watch on big screens outside the civic center. I literally was one of the last 20 people that they let inside.
