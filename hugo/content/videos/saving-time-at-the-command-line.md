---
title: "Saving Time At The Command Line"
image: images/thumbs/0434.jpg
date: Mon, 19 Aug 2019 23:25:00 +0000
author: Derek Taylor
tags: ["command line", "terminal"]
---

#### VIDEO

{{< amazon src="Saving+Time+At+The+Command+Line.mp4" >}}
&nbsp;

#### SHOW NOTES

Today I am going to share a few basic command line tips for increasing your speed in the terminal.
REFERENCED:
+ ► history
+ ► !command
+ ► sudo !!
+ ► CTRL+R (for searching)
+ ► command one; command two
+ ► command one &amp;&amp; command two
+ ► less /path/to/filename
+ ► cat filename | grep search_string
+ ► grep search_string filename
