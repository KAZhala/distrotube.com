---
title: "Monday is Fun Day (Part Two) - DT Live"
image: images/thumbs/0499.jpg
date: 2019-10-21T12:22:40+06:00
author: Derek Taylor
tags: ["Live Stream", "Emacs"]
---

#### VIDEO

{{< amazon src="Monday+is+Fun+Day+(Part+Two)+-+DT+Live-Hrtn5VDu-M0.mp4" >}}
&nbsp;

#### SHOW NOTES

Today's live stream may or may not include: a distro review or two, a closer look at emacs, some Linux-y news, and me interacting with you guys in the chat.
