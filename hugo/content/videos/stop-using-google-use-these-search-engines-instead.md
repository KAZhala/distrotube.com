---
title: "Stop Using Google! Use These Search Engines Instead."
image: images/thumbs/0559.jpg
date: 2020-03-01T12:22:40+06:00
author: Derek Taylor
tags: ["Privacy", "Google"]
---

#### VIDEO

{{< amazon src="Stop+Using+Google!+Use+These+Search+Engines+Instead.mp4" >}}
&nbsp;

#### SHOW NOTES

Do you use the Google search engine?  If you do, you really should switch to something else, because Google tracks everything you do in their search engine.  They are collecting your information; they are data mining you.  Wouldn't you prefer to use a search engine that respects your privacy and doesn't track you?  Here are nine privacy-centric search engines for you to consider.

REFERENCED:
+ https://duckduckgo.com/
+ https://www.startpage.com/
+ https://www.qwant.com/
+ https://www.wolframalpha.com/
+ https://swisscows.com/
+ https://metager.org/
+ https://www.mojeek.com/
+ https://gibiru.com/
+ https://yacy.net/
