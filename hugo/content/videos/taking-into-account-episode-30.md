---
title: "Taking Into Account, Ep. 30 - Linux Life Support, Snap Store, WSL, Chrome OS, Raspberry Pi"
image: images/thumbs/0355.jpg
date: Fri, 22 Feb 2019 22:59:58 +0000
author: Derek Taylor
tags: ["Taking Into Account", ""]
---

#### VIDEO

{{< amazon src="Taking+Into+Account+Ep+30.mp4" >}}
&nbsp;

#### SHOW NOTES

On this edition of Taking Into Account: 

<a href="https://www.youtube.com/watch?v=qgp0SKtTJfk&amp;t=37s">0:37 Linux gaming is on a life-support system called Steam. Valve has the power to keep Linux alive. 

<a href="https://www.youtube.com/watch?v=qgp0SKtTJfk&amp;t=611s">10:11 Canonical is planning some awesome new content for the Snap Store. 

<a href="https://www.youtube.com/watch?v=qgp0SKtTJfk&amp;t=976s">16:16 Next Windows update brings better Linux integration.  Will soon allow direct access to Linux files. 

<a href="https://www.youtube.com/watch?v=qgp0SKtTJfk&amp;t=1153s">19:13 Chrome OS 74 bringing audio support to Linux apps on Chromebooks. 

<a href="https://www.youtube.com/watch?v=qgp0SKtTJfk&amp;t=1270s">21:10 Five of the best Linux distros for the Raspberry Pi. 

<a href="https://www.youtube.com/watch?v=qgp0SKtTJfk&amp;t=1633s">27:13 I read a viewer question regarding Gentoo. 

📰 REFERENCED: 

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=qgp0SKtTJfk&amp;event=video_description&amp;redir_token=wzZk2j02njBfPMF29DXhWFVzajR8MTU1MzY0MTIwN0AxNTUzNTU0ODA3&amp;q=https%3A%2F%2Fwww.engadget.com%2F2019%2F02%2F19%2Flinux-gaming-steam-valve-epic-games-store%2F" target="_blank">https://www.engadget.com/2019/02/19/l...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=qgp0SKtTJfk&amp;event=video_description&amp;redir_token=wzZk2j02njBfPMF29DXhWFVzajR8MTU1MzY0MTIwN0AxNTUzNTU0ODA3&amp;q=https%3A%2F%2Fwww.forbes.com%2Fsites%2Fjasonevangelho%2F2019%2F02%2F20%2Fcanonical-is-planning-some-awesome-new-content-for-the-snap-store%2F" target="_blank">https://www.forbes.com/sites/jasoneva...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=qgp0SKtTJfk&amp;event=video_description&amp;redir_token=wzZk2j02njBfPMF29DXhWFVzajR8MTU1MzY0MTIwN0AxNTUzNTU0ODA3&amp;q=https%3A%2F%2Fwww.zdnet.com%2Farticle%2Fnext-windows-update-brings-better-linux-integration%2F" target="_blank">https://www.zdnet.com/article/next-wi...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=qgp0SKtTJfk&amp;event=video_description&amp;redir_token=wzZk2j02njBfPMF29DXhWFVzajR8MTU1MzY0MTIwN0AxNTUzNTU0ODA3&amp;q=https%3A%2F%2Fwww.aboutchromebooks.com%2Fnews%2Fchrome-os-74-audio-playback-support-linux-on-chromebooks-project-crostini%2F" target="_blank">https://www.aboutchromebooks.com/news...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=qgp0SKtTJfk&amp;event=video_description&amp;redir_token=wzZk2j02njBfPMF29DXhWFVzajR8MTU1MzY0MTIwN0AxNTUzNTU0ODA3&amp;q=https%3A%2F%2Fwww.maketecheasier.com%2Fbest-linux-distros-raspberry-pi%2F" target="_blank">https://www.maketecheasier.com/best-l...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=qgp0SKtTJfk&amp;event=video_description&amp;redir_token=wzZk2j02njBfPMF29DXhWFVzajR8MTU1MzY0MTIwN0AxNTUzNTU0ODA3&amp;q=https%3A%2F%2Fwiki.gentoo.org%2Fwiki%2FBenefits_of_Gentoo" target="_blank">https://wiki.gentoo.org/wiki/Benefits...
