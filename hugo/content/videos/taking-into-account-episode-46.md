---
title: "Taking Into Account, Ep. 46 (LIVE)"
image: images/thumbs/0414.jpg
date: Thu, 20 Jun 2019 05:27:00 +0000
author: Derek Taylor
tags: ["Taking Into Account", "Live Stream"]
---

#### VIDEO

{{< amazon src="Taking+Into+Account%2C+Ep.+46+(LIVE)-FOCg9XS7Svk.f299.mp4.part" >}}
&nbsp;

#### SHOW NOTES

This week's edition of Taking Into Account...LIVE! We discuss some of the latest stories in the Linux-sphere including "What's the point of Ubuntu in 2020?" and some recent security flaws in Linux.


&nbsp;
#### REFERENCED:
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.zdnet.com%2Farticle%2Fopen-invention-network-the-linux-based-patent-non-aggression-community-exceeds-3000-licensees%2F&amp;v=FOCg9XS7Svk&amp;event=video_description&amp;redir_token=O83AZe7funQHjWq42os0cNVUjSR8MTU3NTY5NjQ4M0AxNTc1NjEwMDgz" target="_blank" rel="nofollow noopener noreferrer">https://www.zdnet.com/article/open-in...
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.reddit.com%2Fr%2Flinux%2Fcomments%2Fc2d9r4%2Fwhats_the_point_of_ubuntu_in_2020%2F&amp;v=FOCg9XS7Svk&amp;event=video_description&amp;redir_token=O83AZe7funQHjWq42os0cNVUjSR8MTU3NTY5NjQ4M0AxNTc1NjEwMDgz" target="_blank" rel="nofollow noopener noreferrer">https://www.reddit.com/r/linux/commen...
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.techrepublic.com%2Farticle%2Flenovo-shipping-ubuntu-linux-on-2019-thinkpad-p-series-models%2F&amp;v=FOCg9XS7Svk&amp;event=video_description&amp;redir_token=O83AZe7funQHjWq42os0cNVUjSR8MTU3NTY5NjQ4M0AxNTc1NjEwMDgz" target="_blank" rel="nofollow noopener noreferrer">https://www.techrepublic.com/article/...
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fblog.dxmtechsupport.com.au%2Finfographic-linux-vs-windows-for-small-business-productivity%2F&amp;v=FOCg9XS7Svk&amp;event=video_description&amp;redir_token=O83AZe7funQHjWq42os0cNVUjSR8MTU3NTY5NjQ4M0AxNTc1NjEwMDgz" target="_blank" rel="nofollow noopener noreferrer">https://blog.dxmtechsupport.com.au/in...
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.zdnet.com%2Farticle%2Fnetflix-to-linux-users-patch-sack-panic-kernel-bug-now-to-stop-remote-attacks%2F&amp;v=FOCg9XS7Svk&amp;event=video_description&amp;redir_token=O83AZe7funQHjWq42os0cNVUjSR8MTU3NTY5NjQ4M0AxNTc1NjEwMDgz" target="_blank" rel="nofollow noopener noreferrer">https://www.zdnet.com/article/netflix...
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.zdnet.com%2Farticle%2Fmicrosoft-warns-azure-customers-of-exim-worm%2F&amp;v=FOCg9XS7Svk&amp;event=video_description&amp;redir_token=O83AZe7funQHjWq42os0cNVUjSR8MTU3NTY5NjQ4M0AxNTc1NjEwMDgz" target="_blank" rel="nofollow noopener noreferrer">https://www.zdnet.com/article/microso...
