---
title: "Windows Marketshare Declines While Ubuntu Numbers Up 700 Percent!"
image: images/thumbs/0615.jpg
date: 2020-05-06T12:23:40+06:00
author: Derek Taylor
tags: ["Windows", "Ubuntu"]
---

#### VIDEO

{{< amazon src="+Windows+Marketshare+In+Decline+While+Ubuntu+Numbers+Up+700+Percent.mp4" >}}
&nbsp;

#### SHOW NOTES

Since the pandemic started, Microsoft Windows has seen a decline in marketshare while alternative operating systems such as MacOS and Linux are seeing greater adoption.  Also, Microsoft is starting to port some of it's software over to Linux including the Edge browser and the Office suite.

REFERENCED:
+ https://www.techradar.com/news/bad-news-for-windows-10-as-users-shift-to-ubuntu-and-macos