---
title: "Tiny Core Linux - Sometimes Size Does Matter"
image: images/thumbs/0240.jpg
date: Tue, 03 Jul 2018 00:19:33 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Tiny Core"]
---

#### VIDEO

{{< amazon src="Tiny+Core+Linux+-+Sometimes+Size+Does+Matter.mp4" >}}
&nbsp;

#### SHOW NOTES

Today I'm taking a look at Tiny Core Linux. The standard Tiny Core has an image size of 16MB. That includes a graphical user interface! Almost no CPU usage and used around 50MB of RAM during this video. For the ultimate minimalist.
