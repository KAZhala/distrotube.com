---
title: "An Introduction to DistroTube"
image: images/thumbs/0453.jpg
date: Tue, 29 Oct 2019 00:03:00 +0000
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

{{< amazon src="An+Introduction+To+DistroTube.mp4" >}}
&nbsp;

#### SHOW NOTES

This video serves as an introduction to my channel. An introduction video is useful to have posted on your YouTube channel and your Patreon.
