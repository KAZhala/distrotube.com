---
title: "Mageia 6.1 KDE Installation and First Look"
image: images/thumbs/0293.jpg
date: Sat, 06 Oct 2018 19:57:24 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Mageia"]
---

#### VIDEO

{{< amazon src="Mageia+61+KDE+Installation+and+First+Look.mp4" >}}
&nbsp;

#### SHOW NOTES

Today, I'm taking a look at Mageia 6.1 KDE.  I've never looked at Mageia
 before on the channel...or off the channel.  I know nothing about it.  
This should be fun!

<a href="https://www.youtube.com/redirect?redir_token=JvL5zegN9fwdsrK9XkTpJ3l9ttR8MTU1MzQ1NzQ2MkAxNTUzMzcxMDYy&amp;event=video_description&amp;v=6GgQS5XWJ8s&amp;q=https%3A%2F%2Fwww.mageia.org%2Fen%2F" rel="noreferrer noopener" target="_blank">https://www.mageia.org/en/
