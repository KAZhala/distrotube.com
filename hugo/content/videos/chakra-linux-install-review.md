---
title: "Chakra Linux First Impression Install & Review"
image: images/thumbs/0038.jpg
date: Tue, 21 Nov 2017 14:20:41 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Chakra", "KDE"]
---

#### VIDEO

{{< amazon src="Chakra+Linux+First+Impression+Install+%26+Review.mp4" >}}  
&nbsp;

#### SHOW NOTES

I take a look at yet another Arch-based Linux distribution--Chakra. It is a distro focused on the KDE desktop and Qt technologies and features a rather unique half-rolling release model. <a href="https://chakralinux.org/">https://chakralinux.org/</a>
