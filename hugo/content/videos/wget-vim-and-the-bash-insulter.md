---
title: "Wget, Vim and The Bash Insulter"
image: images/thumbs/0539.jpg
date: 2020-02-09T12:22:40+06:00
author: Derek Taylor
tags: ["terminal", "command line", "Shell Scripting"]
---

#### VIDEO

{{< amazon src="Wget%2C+Vim+and+The+Bash+Insulter.mp4" >}}
&nbsp;

#### SHOW NOTES

I recently came across a script called Bash Insulter.  It chooses a random insult every time you enter a wrong command.  It's a nice, simple script.  So let's hack on it a little.  

REFERENCED:
+  https://www.gnu.org/software/wget/ - Wget
+  https://www.vim.org/ - Vim
+  https://www.gnu.org/software/emacs/ - Emacs
+  https://github.com/hkbakke/bash-insulter - Bash Insulter