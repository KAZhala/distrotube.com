---
title: "Live May 3, 2018 - My 200th Video and a New Graphics Card"
image: images/thumbs/0194.jpg
date: Thu, 03 May 2018 22:56:06 +0000
author: Derek Taylor
tags: ["Live Stream", ""]
---

#### VIDEO

{{< amazon src="Live+May+3%2C+2018+-+My+200th+Video+and+a+New+Graphics+Card.mp4" >}}
&nbsp;

#### SHOW NOTES

This episode marks a minor milestone for my channel--my 200th video in a little less than seven months. In this stream, I'll discuss some recent purchases I've made to help improve the channel. I may also talk about my experiences living in tiling WMs this past month. Also, I may discuss my future as far as social media, plus other randomness. 
