---
title: "Deepin 15.6 Installation and First Look"
image: images/thumbs/0231.jpg
date: Sat, 16 Jun 2018 00:06:32 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Deepin"]
---

#### VIDEO

{{< amazon src="Deepin+15.6+Installation+and+First+Look.mp4" >}}
&nbsp;

#### SHOW NOTES

Taking a quick look at the newly released Deepin 15.6.
