---
title: "Ubuntu MATE 18.04 'Bionic Beaver' Install and Review"
image: images/thumbs/0189.jpg
date: Fri, 27 Apr 2018 22:50:01 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Ubuntu MATE"]
---

#### VIDEO

{{< amazon src="Ubuntu+MATE+18.04+Bionic+Beaver+Install+and+Review.mp4" >}}
&nbsp;

#### SHOW NOTES

It's release week for the various flavors of Ubuntu 18.04. Today, I'm looking at possibly the most popular flavor of Ubuntu that isn't their flagship edition--Ubuntu MATE 18.04 "Bionic Beaver". Ubuntu MATE offers a variety different desktop layouts and is supported for up to three years. http://ubuntu-mate.org/
