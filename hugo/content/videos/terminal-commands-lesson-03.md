---
title: "Terminal Commands Lesson 03 - which, whereis, locate, find"
image: images/thumbs/0025.jpg
date: Sun, 29 Oct 2017 02:29:04 +0000
author: Derek Taylor
tags: ["Terminal", "Command Line"]
---

#### VIDEO

{{< amazon src="Terminal+Commands+Lesson+03+-+which%2C+whereis%2C+locate%2C+find.mp4" >}}  
&nbsp;

#### SHOW NOTES

In this video on terminal commands and their usage I take a look at a few commands you can use to search for programs on your system and search for files. I discuss the which, whereis, locate and find commands.
