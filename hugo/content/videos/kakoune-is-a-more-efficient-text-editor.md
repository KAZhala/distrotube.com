---
title: "Kakoune Is A More Efficient Text Editor"
image: images/thumbs/0629.jpg
date: 2020-05-23T12:23:40+06:00
author: Derek Taylor
tags: ["TUI Apps", "Kakoune"]
---

#### VIDEO

{{< amazon src="Kakoune+Is+A+More+Efficient+Text+Editor.mp4" >}}
&nbsp;

#### SHOW NOTES

The age old question is what's the best text editor: Vim or Emacs?  Well, there's a third editor that needs to be considered. And no, it's not Nano.  I'm talking about Kakoune, a modal text editor that claims to be more efficient by using less keystrokes and being less mistake-prone.

REFERENCED:
+ http://kakoune.org/ - Kakoune Website
+ https://github.com/mawww/kakoune/blob/master/contrib/TRAMPOLINE - Trampoline Tutorial