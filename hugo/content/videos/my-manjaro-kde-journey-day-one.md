---
title: "My Manjaro KDE Journey, Day One"
image: images/thumbs/0074.jpg
date: Fri, 29 Dec 2017 15:56:57 +0000
author: Derek Taylor
tags: ["Manjaro", "KDE"]
---

#### VIDEO

{{< amazon src="My+Manjaro+KDE+Journey%2C+Day+One.mp4" >}}  
&nbsp;

#### SHOW NOTES

So I wiped out my installation of Ubuntu LTS on my main machine last night and installed Manjaro KDE. Here's how it is going so far.
