---
title: "Lubuntu 18.04 'Bionic Beaver' Install and Review"
image: images/thumbs/0191.jpg
date: Mon, 30 Apr 2018 22:52:10 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Lubuntu"]
---

#### VIDEO

{{< amazon src="Lubuntu+18.04+Bionic+Beaver+Install+and+Review.mp4" >}}
&nbsp;

#### SHOW NOTES

It's release week for the various flavors of Ubuntu 18.04. Today, I'm looking at Lubuntu 18.04 "Bionic Beaver". Lubuntu 18.04 sports the light and fast LXDE desktop environment and offers support for up to three years. https://lubuntu.me/
