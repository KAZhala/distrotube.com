---
title: "Obscure Window Manager Project - JWM"
image: images/thumbs/0109.jpg
date: Wed, 07 Feb 2018 01:46:12 +0000
author: Derek Taylor
tags: ["jwm", ""]
---

#### VIDEO

{{< amazon src="Obscure+Window+Manager+Project+-+JWM.mp4" >}}
&nbsp;

#### SHOW NOTES

In this video, I take a look at JWM (Joe's Window Manager). JWM is a lightweight floating window manager that is similar in look and feel to Openbox and Fluxbox. It is very configurable and blazing fast. REFERENCES: JWM website: <a href="https://joewing.net/projects/jwm/">https://joewing.net/projects/jwm/</a>     The completed config files can be found on my GitLab page: <a href="https://gitlab.com/dwt1/dotfiles">https://gitlab.com/dwt1/dotfiles</a>
