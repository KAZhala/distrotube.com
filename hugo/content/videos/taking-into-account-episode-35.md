---
title: "Taking Into Account, Ep. 35 - Linux Foundation, Article 13, NexDock 2, Sway, GNOME"
image: images/thumbs/0373.jpg
date: Thu, 28 Mar 2019 16:28:57 +0000
author: Derek Taylor
tags: ["Taking Into Account", ""]
---

#### VIDEO

{{< amazon src="Taking+Into+Account+Ep+35.mp4" >}}
&nbsp;

#### SHOW NOTES

On this edition of Taking Into Account: 

<a href="https://www.youtube.com/watch?v=Rn1N1IxzmLs&amp;t=44s">0:44 The Linux Foundation is not about Linux. Is the LF is all about big business and big money? 

<a href="https://www.youtube.com/watch?v=Rn1N1IxzmLs&amp;t=568s">9:28 Controversial online copyright law Article 13 passed by EU Parliament. 

<a href="https://www.youtube.com/watch?v=Rn1N1IxzmLs&amp;t=887s">14:47 Turn your Smartphone or Pi into a laptop with the NexDock 2.  Now live on Kickstarter. 

<a href="https://www.youtube.com/watch?v=Rn1N1IxzmLs&amp;t=1138s">18:58 Sway is a tiling window manager specially crafted for Wayland. Worth looking at? 

<a href="https://www.youtube.com/watch?v=Rn1N1IxzmLs&amp;t=1410s">23:30 A bit of GNOME news.  Draw on your screen extension released, plus GNOME 3.30 w/o systemd. 

<a href="https://www.youtube.com/watch?v=Rn1N1IxzmLs&amp;t=1658s">27:38 Some of you guys had trouble installing curseradio. 

📰 REFERENCED: 

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=http%3A%2F%2Ftechrights.org%2F2019%2F03%2F26%2Fthe-linux-foundation-is-not-about-linux%2F&amp;v=Rn1N1IxzmLs&amp;redir_token=tcMfeP2kUbeO6W9cNxWNbhlPIut8MTU1NDA0OTc0NkAxNTUzOTYzMzQ2&amp;event=video_description" target="_blank">http://techrights.org/2019/03/26/the-...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Ffossbytes.com%2Farticle-13-passed-eu-parliament%2F&amp;v=Rn1N1IxzmLs&amp;redir_token=tcMfeP2kUbeO6W9cNxWNbhlPIut8MTU1NDA0OTc0NkAxNTUzOTYzMzQ2&amp;event=video_description" target="_blank">https://fossbytes.com/article-13-pass...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.omgubuntu.co.uk%2F2019%2F03%2Fnexdock-2-kickstarter&amp;v=Rn1N1IxzmLs&amp;redir_token=tcMfeP2kUbeO6W9cNxWNbhlPIut8MTU1NDA0OTc0NkAxNTUzOTYzMzQ2&amp;event=video_description" target="_blank">https://www.omgubuntu.co.uk/2019/03/n...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fitsfoss.com%2Fsway-window-manager%2F&amp;v=Rn1N1IxzmLs&amp;redir_token=tcMfeP2kUbeO6W9cNxWNbhlPIut8MTU1NDA0OTc0NkAxNTUzOTYzMzQ2&amp;event=video_description" target="_blank">https://itsfoss.com/sway-window-manager/

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.omgubuntu.co.uk%2F2019%2F03%2Fdraw-on-your-screen-ubuntu-linux&amp;v=Rn1N1IxzmLs&amp;redir_token=tcMfeP2kUbeO6W9cNxWNbhlPIut8MTU1NDA0OTc0NkAxNTUzOTYzMzQ2&amp;event=video_description" target="_blank">https://www.omgubuntu.co.uk/2019/03/d...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.gentoo.org%2Fnews%2F2019%2F03%2F27%2Fgnome-330-openrc.html&amp;v=Rn1N1IxzmLs&amp;redir_token=tcMfeP2kUbeO6W9cNxWNbhlPIut8MTU1NDA0OTc0NkAxNTUzOTYzMzQ2&amp;event=video_description" target="_blank">https://www.gentoo.org/news/2019/03/2...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fgithub.com%2Fchronitis%2Fcurseradio&amp;v=Rn1N1IxzmLs&amp;redir_token=tcMfeP2kUbeO6W9cNxWNbhlPIut8MTU1NDA0OTc0NkAxNTUzOTYzMzQ2&amp;event=video_description" target="_blank">https://github.com/chronitis/curseradio
