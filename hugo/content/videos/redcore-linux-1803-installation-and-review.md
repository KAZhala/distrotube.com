---
title: "Redcore Linux 1803 Installation and Review"
image: images/thumbs/0178.jpg
date: Sun, 15 Apr 2018 22:33:38 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Redcore Linux", "LXQt"]
---

#### VIDEO

{{< amazon src="Redcore+Linux+1803+Installation+and+Review.mp4" >}}
&nbsp;

#### SHOW NOTES

In this video, I do a quick install and overview of the recently released Redcore Linux 1803. Redcore is a Gentoo-based Linux distribution that features the LXQt desktop environment. <a href="https://redcorelinux.org/">https://redcorelinux.org/</a>
