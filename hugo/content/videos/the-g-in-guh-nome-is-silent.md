---
title: "The G in GNOME is silent and Linux is just a kernel"
image: images/thumbs/0114.jpg
date: Thu, 15 Feb 2018 01:53:24 +0000
author: Derek Taylor
tags: ["GNOME", ""]
---

#### VIDEO

{{< amazon src="The+G+in+GNOME+is+silent+and+Linux+is+just+a+kernel.mp4" >}}  
&nbsp;

#### SHOW NOTES

Been getting a lot of viewer feedback about how I mispronounce certain words. So let's set a few things straight. 
