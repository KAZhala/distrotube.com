---
title: "Windows Does It Better Than Linux"
image: images/thumbs/0131.jpg
date: Sat, 03 Mar 2018 21:19:12 +0000
author: Derek Taylor
tags: ["Windows", ""]
---

#### VIDEO

{{< amazon src="Windows+Does+It+Better+Than+Linux.mp4" >}}  
&nbsp;

#### SHOW NOTES

After living in Linux exclusively for a decade, I have come to the conclusion that there are some things that Windows simply does BETTER than Linux. An objective look at the areas Windows dominates Linux.
