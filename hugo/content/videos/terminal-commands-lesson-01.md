---
title: "Terminal Commands Lesson 01 - pwd, cd, ls, man, --help"
image: images/thumbs/0002.jpg
date: Sun, 08 Oct 2017 01:34:14 +0000
author: Derek Taylor
tags: ["Terminal", "Command Line"]
---

#### VIDEO

{{< amazon src="Terminal+Commands+Lesson+01+-+pwd%2C+cd%2C+ls%2C+man%2C+--help.mp4" >}}  
&nbsp;

#### SHOW NOTES

In our first tutorial on terminal commands and their usage, we will discuss the following: pwd, cd, ls, man...and we will also talk about the --help flag. We learn how to change directories and move around in our system, how to list contents of directories, and (most importantly) how to view manpages.
