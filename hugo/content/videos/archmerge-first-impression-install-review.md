---
title: "ArchMerge First Impression Install & Review"
image: images/thumbs/0054.jpg
date: Fri, 08 Dec 2017 14:47:04 +0000
author: Derek Taylor
tags: ["Distro Reviews", "ArchMerge"]
---

#### VIDEO

{{< amazon src="ArchMerge+First+Impression+Install+%26+Review.mp4" >}}  
&nbsp;

#### SHOW NOTES

ArchMerge is an Arch-based Linux distro that comes pre-installed with the XFCE desktop environment, the Openbox window manager, and the i3 tiling window manager. It is a100% compatible with Arch. https://archmerge.com/
