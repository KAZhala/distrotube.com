---
title: "Categorizing Linux Distros"
image: images/thumbs/0512.jpg
date: 2020-01-11T12:23:40+06:00
author: Derek Taylor
tags: ["Linux Memes", ""]
---

#### VIDEO

{{< amazon src="Categorizing+Linux+Distros.mp4" >}}
&nbsp;

#### SHOW NOTES

I often use some terms when I describe the various Linux distributions.  Some of these terms, I've not taken the time to properly define.  So I discuss five categories of Linux distros...at least, according to DT.
