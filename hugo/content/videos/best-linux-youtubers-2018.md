---
title: "Listing some of the best Linux Youtubers in 2018. Who did I miss?"
image: images/thumbs/0086.jpg
date: Thu, 11 Jan 2018 01:14:12 +0000
author: Derek Taylor
tags: ["Youtubers", ""]
---

#### VIDEO

{{< amazon src="Listing+some+of+the+best+Linux+Youtubers+in+2018.+Who+did+I+miss.mp4" >}}  
&nbsp;

#### SHOW NOTES

A brief video where I discuss the Youtube channels to which I'm subscribed. I talk a little bit about my favorites out of that list. I also would love to hear from you guys about your favorite Linux and Open Source related Youtubers that you follow. Hoepfully, we bring a little more recognition to some of these Youtubers.
