---
title: "Managing Your Splits In Vim"
image: images/thumbs/0603.jpg
date: 2020-04-21T12:23:40+06:00
author: Derek Taylor
tags: ["Vim", "", ""]
---

#### VIDEO

{{< amazon src="Managing+Your+Splits+In+Vim.mp4" >}}
&nbsp;

#### SHOW NOTES

In this video, I cover some of the basics regarding splits in Vim.  I show you some of my settings in my vimrc that makes some things easier when working with splits.

REFERENCED:
+ https://gitlab.com/dwt1/dotfiles/-/blob/master/.config/nvim/init.vim - My Neovim config
