---
title: "Gecko Linux 423 Cinnamon Install & Review"
image: images/thumbs/0084.jpg
date: Tue, 09 Jan 2018 01:11:39 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Gecko Linux", "Cinnamon"]
---

#### VIDEO

{{< amazon src="Gecko+Linux+423+CInnamon+Install+%26+Review.mp4" >}}  
&nbsp;

#### SHOW NOTES

Gecko Linux is an OpenSUSE-based Linux distro that includes some non-free software that OpenSUSE cannot include by default. In this video, I take Gecko Linux's Cinnamon edition for a spin. <a href="http://geckolinux.github.io/">http://geckolinux.github.io/</a>
