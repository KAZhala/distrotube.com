---
title: "Live Mar 29, 2018 - Updating all my VMs"
image: images/thumbs/0163.jpg
date: Thu, 29 Mar 2018 22:13:24 +0000
author: Derek Taylor
tags: ["Live Stream", ""]
---

#### VIDEO

{{< amazon src="Live+Mar+29%2C+2018+-+Updating+all+my+VMs.mp4" >}}  
&nbsp;

#### SHOW NOTES

In this live stream, I'm going to login to all of my virtual machines and update them. While we wait on updates, I may show off some of these VMs I'm updating. I may also discuss my experience so far using Calculate Linux on my main machine. 
