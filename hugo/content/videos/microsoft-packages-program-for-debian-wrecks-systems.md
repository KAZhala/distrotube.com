---
title: "Microsoft Packages Program for Debian Which Could Wreck Your System"
image: images/thumbs/0229.jpg
date: Thu, 14 Jun 2018 00:00:59 +0000
author: Derek Taylor
tags: ["Microsoft", ""]
---

#### VIDEO

{{< amazon src="Microsoft+Packages+Program+for+Debian+Which+Could+Wreck+Your+System.mp4" >}}
&nbsp;

#### SHOW NOTES

Microsoft makes a .deb package that could break your Linux system.

