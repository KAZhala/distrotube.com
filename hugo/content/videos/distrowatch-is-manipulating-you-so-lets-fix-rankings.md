---
title: "DistroWatch Is Manipulating You, So Let's Fix The Rankings!"
image: images/thumbs/0549.jpg
date: 2020-02-19T12:22:40+06:00
author: Derek Taylor
tags: ["DistroWatch", ""]
---

#### VIDEO

{{< amazon src="DistroWatch+Is+Manipulating+You%2C+So+Let's+Fix+The+Rankings.mp4" >}}
&nbsp;

#### SHOW NOTES

DistroWatch ranking suck.  They don't actually measure Linux popularity but DistroWatch doesn't mind if people mistakenly think so.  After all, having a ranking based on page hits to their site....well, that drives traffic to their site allowing them to charge more to potential advertisers.  In short, they are using the people that don't understand how the rankings work to make money.

So let's send them a message.  Since their rankings are so easy to "game," let's make Arch Linux (currently #18) the new number one distro on DistroWatch!  How can you help?  Please do the following:

(1) Make https://distrowatch.com/table.php?dis... your web browsers homepage.
(2) Also, make the Arch DistroWatch page your mobile phone's browser homepage.
(3) Share the link to the Arch DistroWatch page on social media sites like Facebook, Twitter, Mastodon, Reddit, Instagram, etc.
(4) Drop the link in web forums, blog posts, emails, etc.

REFERENCED:
+ https://distrowatch.com/table.php?dis... - CLICK THIS LINK