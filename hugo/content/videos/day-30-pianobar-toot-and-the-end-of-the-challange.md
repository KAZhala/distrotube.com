---
title: "Day 30 - Pianobar, Toot and the End of the Challenge"
image: images/thumbs/0203.jpg
date: Sat, 12 May 2018 23:08:35 +0000
author: Derek Taylor
tags: ["terminal", ""]
---

#### VIDEO

{{< amazon src="Day+30+-+Pianobar%2C+Toot+and+the+End+of+the+Challenge.mp4" >}}
&nbsp;

#### SHOW NOTES

Day 30 of my 30 challenge. I'm living in a tiling window manager for 30 days and only using terminal-based applications where possible. So I'm on the last day of the challenge and am still discovering cool terminal-based programs. Now that the challenge is over, am I going to run back to Openbox or will I continue living in a tiler? 

https://github.com/PromyLOPh/pianobar

https://github.com/ihabunek/toot
