---
title: "Make Reinstalling Linux Faster With A Package List"
image: images/thumbs/0544.jpg
date: 2020-02-14T12:22:40+06:00
author: Derek Taylor
tags: ["terminal", "command line", "Arch Linux"]
---

#### VIDEO

{{< amazon src="Make+Reinstalling+Linux+Faster+With+A+Package+List.mp4" >}}
&nbsp;

#### SHOW NOTES

Sometimes you would like to reinstall your Linux distribution but dread the idea of having to reinstall the hundreds or thousands of programs that you need.  This process can be streamlined and made much faster by simply leveraging the power of your package manager and keeping a list of the packages that you have explicitly installed on your machine.

ERRATA:
If you are on Debian/Ubuntu:
+  apt list --installed (lists everything installed)
+  apt list --manual-installed  (lists explicit installed)

REFERENCED:
+  https://wiki.archlinux.org/index.php/Pacman/Tips_and_tricks#List_of_installed_packages