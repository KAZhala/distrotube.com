---
title: "A Quick Look At CloverOS GNU/Linux"
image: images/thumbs/0248.jpg
date: Sun, 22 Jul 2018 00:32:16 +0000
author: Derek Taylor
tags: ["Distro Reviews", "CloverOS"]
---

#### VIDEO

{{< amazon src="A+Quick+Look+At+CloverOS+GNULinux.mp4" >}}
&nbsp;

#### SHOW NOTES

A quick look at CloverOS GNU/Linux--a Gentoo-based Linux distro with a super quick install. https://cloveros.ga/
