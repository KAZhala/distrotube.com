---
title: "Move Your Home Directory To A Second Drive"
image: images/thumbs/0550.jpg
date: 2020-02-20T12:22:40+06:00
author: Derek Taylor
tags: ["terminal", "command line"]
---

#### VIDEO

{{< amazon src="Move+Your+Home+Directory+To+A+Second+Drive.mp4" >}}
&nbsp;

#### SHOW NOTES

Do you have more than one drive in your computer?  Then you may want to move your /home directory (or other directories) to their own drive.  It certainly makes reinstalling much faster when you don't have to backup all of that data and then move it back onto the machine after the reinstall. 

WARNING!!!
I am not responsible for lost documents, photos, passwords, ssh keys, etc.  Having a backup of the directory that you are doing this with IS RECOMMENDED.  You have been warned!

REFERENCED:
+ https://www.distrotube.com/blog/move-your-home-folder-to-second-drive/