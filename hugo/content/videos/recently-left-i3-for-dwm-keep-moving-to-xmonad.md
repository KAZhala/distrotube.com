---
title: "Recently Left i3 For Dwm? If so, keep moving...to Xmonad!"
image: images/thumbs/0620.jpg
date: 2020-05-11T12:23:40+06:00
author: Derek Taylor
tags: ["Tiling Window Managers", "dwm", "i3", "xmonad"]
---

#### VIDEO

{{< amazon src="Recently+Left+i3+For+Dwm+If+so%2C+keep+moving+to+Xmonad.mp4" >}}
&nbsp;

#### SHOW NOTES

Many Suckless fans would have us all believe that dwm is the most amazing tiling window manager because it does not have a config file written in a user-friendly syntax, which can be limiting.  Instead, dwm's config is the source code.  People seem to think that dwm is special in this regard, but I would argue that dwm isn't particularly special and that some other window managers might even surpass dwm at being "Suckless."

REFERENCED:
+ https://www.youtube.com/watch?v=B5r47Q1cn_o - Check out Luke Smith's video!