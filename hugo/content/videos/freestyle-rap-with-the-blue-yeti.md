---
title: "Freestyle Rap and the Blue Yeti Mic"
image: images/thumbs/0157.jpg
date: Sat, 24 Mar 2018 22:06:15 +0000
author: Derek Taylor
tags: ["Hardware", ""]
---

#### VIDEO

{{< amazon src="Freestyle+Rap+and+the+Blue+Yeti+Mic.mp4" >}}  
&nbsp;

#### SHOW NOTES

Just demonstrating a little bit of what is possible with some creative rhymes and a Blue Yeti microphone. 
