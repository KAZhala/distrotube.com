---
title: "Cancel Culture and Safe Spaces Destroying Linux"
image: images/thumbs/0463.jpg
date: Mon, 11 Nov 2019 00:17:00 +0000
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

{{< amazon src="Cancel+Culture+and+Safe+Spaces+Destroy+Linux.mp4" >}}
&nbsp;

#### SHOW NOTES

The Linux Foundation has disinvited someone from one of their conferences because of their political opinion, citing that they want their conferences to be "safe spaces." If the Linux Foundation wants to be a part of today's "cancel culture", maybe it's time for the Linux community to cancel the Linux Foundation.

#### REFERENCED:
+ ► <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?redir_token=4DIklHqEeaOcQTVFe3vx1LGyABd8MTU3NzQ5MjI3NkAxNTc3NDA1ODc2&amp;q=https%3A%2F%2Freclaimthenet.org%2Flinux-foundation-censorship-kubecon%2F&amp;event=video_description&amp;v=a8TcBeltvk4" target="_blank" rel="nofollow noopener noreferrer">https://reclaimthenet.org/linux-found...
+ ► <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/watch?v=1YO1DTeGxog">https://www.youtube.com/watch?v=1YO1D...
+ ► <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?redir_token=4DIklHqEeaOcQTVFe3vx1LGyABd8MTU3NzQ5MjI3NkAxNTc3NDA1ODc2&amp;q=https%3A%2F%2Fen.wikipedia.org%2Fwiki%2FTone_policing&amp;event=video_description&amp;v=a8TcBeltvk4" target="_blank" rel="nofollow noopener noreferrer">https://en.wikipedia.org/wiki/Tone_po...
+ ► <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?redir_token=4DIklHqEeaOcQTVFe3vx1LGyABd8MTU3NzQ5MjI3NkAxNTc3NDA1ODc2&amp;q=https%3A%2F%2Fwww.urbandictionary.com%2Fdefine.php%3Fterm%3DCancel%2520Culture&amp;event=video_description&amp;v=a8TcBeltvk4" target="_blank" rel="nofollow noopener noreferrer">https://www.urbandictionary.com/defin...
