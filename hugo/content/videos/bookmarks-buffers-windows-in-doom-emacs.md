---
title: "Bookmarks, Buffers and Windows in Doom Emacs"
image: images/thumbs/0525.jpg
date: 2020-01-25T12:22:40+06:00
author: Derek Taylor
tags: ["Emacs", ""]
---

#### VIDEO

{{< amazon src="Bookmarks+Buffers+and+Windows+in+Doom+Emacs.mp4" >}}
&nbsp;

#### SHOW NOTES

I've been using Doom Emacs for a couple of months and have enjoyed the journey.  In this video, I will discuss bookmarks, buffers and windows (splits) in Doom Emacs, as well as showing you some of the keybindings specific to Doom Emacs.

REFERENCED:
+ https://github.com/hlissner/doom-emacs