---
title: "Taking Into Account, Ep. 38 - Windows 10, Open Source Wins, VSCodium, Pengwin, Davinci, Kdenlive"
image: images/thumbs/0384.jpg
date: Fri, 19 Apr 2019 02:56:20 +0000
author: Derek Taylor
tags: ["Taking Into Account", ""]
---

#### VIDEO

{{< amazon src="Taking+Into+Account+Ep+38.mp4" >}}
&nbsp;

#### SHOW NOTES

On this edition of Taking Into Account: 

<a href="https://www.youtube.com/watch?v=vwmfjVyY3IA&amp;t=41s">0:41 Windows 10 is doing a great job persuading people to switch to a different operating system. 

<a href="https://www.youtube.com/watch?v=vwmfjVyY3IA&amp;t=693s">11:33 Open Source has won! Red Hat survey finds we're living in an open-source world. 

<a href="https://www.youtube.com/watch?v=vwmfjVyY3IA&amp;t=995s">16:35 VSCodium is the 100 percent open source version of Microsoft's VS Code.  No data tracking! 

<a href="https://www.youtube.com/watch?v=vwmfjVyY3IA&amp;t=1296s">21:36 Pengwin is a Linux specifically for Windows Subsystem for Linux. 

<a href="https://www.youtube.com/watch?v=vwmfjVyY3IA&amp;t=1464s">24:24 Some new video editor releases.  Davinci Resolve 16 Beta and Kdenlive 19.04 are released. 

<a href="https://www.youtube.com/watch?v=vwmfjVyY3IA&amp;t=1655s">27:35 I read a viewer question from Mastodon about Snaps, Flatpaks and Appimages. 

📰 REFERENCED: 

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?event=video_description&amp;q=https%3A%2F%2Fwww.forbes.com%2Fsites%2Fkevinmurnane%2F2019%2F04%2F14%2Fwindows-10-is-doing-a-great-job-persuading-people-to-switch-to-a-different-operating-system%2F&amp;redir_token=MJMKg8hTWNvRPj8FItpDyQaSFdp8MTU1NzE5Nzc4NEAxNTU3MTExMzg0&amp;v=vwmfjVyY3IA" target="_blank">https://www.forbes.com/sites/kevinmur...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?event=video_description&amp;q=https%3A%2F%2Fwww.zdnet.com%2Farticle%2Fred-hat-survey-finds-were-living-in-an-open-source-world%2F&amp;redir_token=MJMKg8hTWNvRPj8FItpDyQaSFdp8MTU1NzE5Nzc4NEAxNTU3MTExMzg0&amp;v=vwmfjVyY3IA" target="_blank">https://www.zdnet.com/article/red-hat...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?event=video_description&amp;q=https%3A%2F%2Fitsfoss.com%2Fvscodium%2F&amp;redir_token=MJMKg8hTWNvRPj8FItpDyQaSFdp8MTU1NzE5Nzc4NEAxNTU3MTExMzg0&amp;v=vwmfjVyY3IA" target="_blank">https://itsfoss.com/vscodium/

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?event=video_description&amp;q=https%3A%2F%2Fwww.zdnet.com%2Farticle%2Fpengwin-a-linux-specifically-for-windows-subsystem-for-linux%2F&amp;redir_token=MJMKg8hTWNvRPj8FItpDyQaSFdp8MTU1NzE5Nzc4NEAxNTU3MTExMzg0&amp;v=vwmfjVyY3IA" target="_blank">https://www.zdnet.com/article/pengwin...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?event=video_description&amp;q=https%3A%2F%2Fwww.linuxuprising.com%2F2019%2F04%2Fhollywood-grade-video-editor-davinci.html&amp;redir_token=MJMKg8hTWNvRPj8FItpDyQaSFdp8MTU1NzE5Nzc4NEAxNTU3MTExMzg0&amp;v=vwmfjVyY3IA" target="_blank">https://www.linuxuprising.com/2019/04...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?event=video_description&amp;q=https%3A%2F%2Fwww.omgubuntu.co.uk%2F2019%2F04%2Fkdenlive-video-editor-released&amp;redir_token=MJMKg8hTWNvRPj8FItpDyQaSFdp8MTU1NzE5Nzc4NEAxNTU3MTExMzg0&amp;v=vwmfjVyY3IA" target="_blank">https://www.omgubuntu.co.uk/2019/04/k...
