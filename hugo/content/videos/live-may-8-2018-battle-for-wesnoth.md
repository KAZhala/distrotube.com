---
title: "Live May 8, 2018 - Battle for Wesnoth"
image: images/thumbs/0200.jpg
date: Tue, 08 May 2018 23:04:50 +0000
author: Derek Taylor
tags: ["Live Stream", "Gaming"]
---

#### VIDEO

{{< amazon src="Live+May+8%2C+2018+-+Battle+for+Wesnoth.mp4" >}}
&nbsp;

#### SHOW NOTES

Watch me play one of the stalwarts of Linux gaming -- Battle for Wesnoth! I begin as a noob. Might go through the turorials. Will definitely stick to the easy campaigns. http://wesnoth.org/
