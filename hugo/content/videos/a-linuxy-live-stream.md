---
title: "A Linuxy Live Stream"
image: images/thumbs/0503.jpg
date: 2019-11-13T12:22:40+06:00
author: Derek Taylor
tags: ["Live Stream", ""]
---

#### VIDEO

{{< amazon src="A+Linuxy+Live+Stream-x_KaB_LmJrc.mp4" >}}
&nbsp;

#### SHOW NOTES

This Linuxy live stream will just be me hanging out with you.  I might do some Linux stuff.  I might talk about some Linux stuff.  I might just do a Q&A with you guys hanging out in the chat.  This stream might be fun and entertaining, or it could be a complete train wreck.  With my streams, you never know what you're gonna get.
