---
title: "Ubuntu MATE 18.10 'Cosmic Cuttlefish' Installation and First Look"
image: images/thumbs/0299.jpg
date: Sat, 20 Oct 2018 23:59:28 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Ubuntu MATE"]
---

#### VIDEO

{{< amazon src="Ubuntu+Mate+1810+Cosmic+Cuttlefish+Installation+and+First+Look.mp4" >}}
&nbsp;

#### SHOW NOTES

Today, I'm taking a quick look at Ubuntu MATE 18.10 "Cosmic Cuttlefish."
  This version of Ubuntu MATE sports the MATE desktop version 1.20.3, 
kernel 4.18, Firefox 63 and LibreOffice 6.   Overall, a modest release 
that focused mostly on bug fixes rather than introducing new features.

<a href="https://www.youtube.com/redirect?q=http%3A%2F%2Fubuntu-mate.org%2Fblog%2Fubuntu-mate-cosmic-final-release%2F&amp;redir_token=2BEjZCLBKPSybwHO8gYB40ja-cV8MTU1MzQ3MTk3OEAxNTUzMzg1NTc4&amp;v=nYr3qJ3I-Jc&amp;event=video_description" rel="noreferrer noopener" target="_blank">http://ubuntu-mate.org/blog/ubuntu-ma...
