---
title: "LXLE 16.04.3 Install & Review"
image: images/thumbs/0040.jpg
date: Thu, 23 Nov 2017 14:23:38 +0000
author: Derek Taylor
tags: ["Distro Reviews", "LXLE"]
---

#### VIDEO

{{< amazon src="LXLE+16.04.3+Install+%26+Review.mp4" >}}  
&nbsp;

#### SHOW NOTES

I take a look at this Ubuntu/Lubuntu-based Linux distribution called LXLE. It is based on the LTS of Ubuntu so it is stable. It boasts a nice collection of programs installed by default as well as some nice theming options and a huge collection of wallpapers. <a href="http://www.lxle.net/">http://www.lxle.net/</a>
