---
title: "Obscure Window Manager Project - IceWM"
image: images/thumbs/0246.jpg
date: Wed, 18 Jul 2018 00:29:30 +0000
author: Derek Taylor
tags: ["IceWM", ""]
---

#### VIDEO

{{< amazon src="Obscure+Window+Manager+Project+-+IceWM.mp4" >}}
&nbsp;

#### SHOW NOTES

This is the last of the original 12 "obscure" window managers that I initially set out to review.  
