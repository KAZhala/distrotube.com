---
title: "The Many Flavors of Ubuntu 19.04 'Disco Dingo'"
image: images/thumbs/0383.jpg
date: Thu, 18 Apr 2019 02:54:59 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Ubuntu"]
---

#### VIDEO

{{< amazon src="The+Many+Flavors+of+Ubuntu+1904+Disco+Dingo.mp4" >}}
&nbsp;

#### SHOW NOTES

It's the big day!  Ubuntu Release Day!  So I downloaded the ISOs for  Ubuntu, Kubuntu, Xubuntu, Lubuntu, Ubuntu Budgie, and Ubuntu MATE 19.04  codenamed "Disco Dingo".  I'm going to take these six distros for a  quick spin. 

📰 REFERENCED: 

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=PWp51BAzn-8&amp;event=video_description&amp;q=https%3A%2F%2Fwww.ubuntu.com%2F&amp;redir_token=aJEoKihapmqhqUd0P8CDrnlpPV58MTU1NzE5NzY4OUAxNTU3MTExMjg5" target="_blank">https://www.ubuntu.com/

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=PWp51BAzn-8&amp;event=video_description&amp;q=https%3A%2F%2Fkubuntu.org%2F&amp;redir_token=aJEoKihapmqhqUd0P8CDrnlpPV58MTU1NzE5NzY4OUAxNTU3MTExMjg5" target="_blank">https://kubuntu.org/

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=PWp51BAzn-8&amp;event=video_description&amp;q=https%3A%2F%2Fxubuntu.org%2F&amp;redir_token=aJEoKihapmqhqUd0P8CDrnlpPV58MTU1NzE5NzY4OUAxNTU3MTExMjg5" target="_blank">https://xubuntu.org/

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=PWp51BAzn-8&amp;event=video_description&amp;q=https%3A%2F%2Flubuntu.me%2F&amp;redir_token=aJEoKihapmqhqUd0P8CDrnlpPV58MTU1NzE5NzY4OUAxNTU3MTExMjg5" target="_blank">https://lubuntu.me/

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=PWp51BAzn-8&amp;event=video_description&amp;q=https%3A%2F%2Fubuntubudgie.org%2F&amp;redir_token=aJEoKihapmqhqUd0P8CDrnlpPV58MTU1NzE5NzY4OUAxNTU3MTExMjg5" target="_blank">https://ubuntubudgie.org/

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=PWp51BAzn-8&amp;event=video_description&amp;q=https%3A%2F%2Fubuntu-mate.org%2F&amp;redir_token=aJEoKihapmqhqUd0P8CDrnlpPV58MTU1NzE5NzY4OUAxNTU3MTExMjg5" target="_blank">https://ubuntu-mate.org/
