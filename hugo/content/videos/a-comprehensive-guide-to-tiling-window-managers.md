---
title: "A Comprehensive Guide To Tiling Window Managers"
image: images/thumbs/0443.jpg
date: Sun, 22 Sep 2019 23:47:00 +0000
author: Derek Taylor
tags: ["tiling window managers", "i3wm", "qtile", "awesome", "xmonad", "herbstluftwm", "bspwm", "dwm"]
---

#### VIDEO

{{< amazon src="A+Comprehensive+Guide+To+Tiling+Window+Managers.mp4" >}}
&nbsp;

#### SHOW NOTES

In this video, I explain what is a tiling window manager and why you might want to use a tiling window manager. I also give you a tour of seven popular tiling window managers that I have used: dwm, xmonad, qtile, awesome, i3, herbstluftwm and bspwm. My configs can be found at my GitLab: <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fgitlab.com%2Fdwt1&amp;redir_token=BXC75B_CDEc6UNHBYM8RfdxiYTh8MTU3NzQ5MDQ5OUAxNTc3NDA0MDk5&amp;v=Obzf9ppODJU&amp;event=video_description" target="_blank" rel="nofollow noopener noreferrer">https://gitlab.com/dwt1


REFERENCED:
+ ► <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fdwm.suckless.org%2F&amp;redir_token=BXC75B_CDEc6UNHBYM8RfdxiYTh8MTU3NzQ5MDQ5OUAxNTc3NDA0MDk5&amp;v=Obzf9ppODJU&amp;event=video_description" target="_blank" rel="nofollow noopener noreferrer">https://dwm.suckless.org/
+ ► <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fxmonad.org%2F&amp;redir_token=BXC75B_CDEc6UNHBYM8RfdxiYTh8MTU3NzQ5MDQ5OUAxNTc3NDA0MDk5&amp;v=Obzf9ppODJU&amp;event=video_description" target="_blank" rel="nofollow noopener noreferrer">https://xmonad.org/
+ ► <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?q=http%3A%2F%2Fwww.qtile.org%2F&amp;redir_token=BXC75B_CDEc6UNHBYM8RfdxiYTh8MTU3NzQ5MDQ5OUAxNTc3NDA0MDk5&amp;v=Obzf9ppODJU&amp;event=video_description" target="_blank" rel="nofollow noopener noreferrer">http://www.qtile.org/
+ ► <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fawesomewm.org%2F&amp;redir_token=BXC75B_CDEc6UNHBYM8RfdxiYTh8MTU3NzQ5MDQ5OUAxNTc3NDA0MDk5&amp;v=Obzf9ppODJU&amp;event=video_description" target="_blank" rel="nofollow noopener noreferrer">https://awesomewm.org/
+ ► <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fi3wm.org%2F&amp;redir_token=BXC75B_CDEc6UNHBYM8RfdxiYTh8MTU3NzQ5MDQ5OUAxNTc3NDA0MDk5&amp;v=Obzf9ppODJU&amp;event=video_description" target="_blank" rel="nofollow noopener noreferrer">https://i3wm.org/
+ ► <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fherbstluftwm.org%2F&amp;redir_token=BXC75B_CDEc6UNHBYM8RfdxiYTh8MTU3NzQ5MDQ5OUAxNTc3NDA0MDk5&amp;v=Obzf9ppODJU&amp;event=video_description" target="_blank" rel="nofollow noopener noreferrer">https://herbstluftwm.org/
+ ► <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fgithub.com%2Fbaskerville%2Fbspwm&amp;redir_token=BXC75B_CDEc6UNHBYM8RfdxiYTh8MTU3NzQ5MDQ5OUAxNTc3NDA0MDk5&amp;v=Obzf9ppODJU&amp;event=video_description" target="_blank" rel="nofollow noopener noreferrer">https://github.com/baskerville/bspwm
