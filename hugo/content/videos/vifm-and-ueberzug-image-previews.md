---
title: "Vifm and Überzug (Ueberzug) Image Previews"
image: images/thumbs/0359.jpg
date: Fri, 01 Mar 2019 23:05:51 +0000
author: Derek Taylor
tags: ["TUI Apps", "vifm"]
---

#### VIDEO

{{< amazon src="Vifm+and+%C3%9Cberzug+(Ueberzug)+Image+Previews.mp4" >}}
&nbsp;

#### SHOW NOTES

I switched to vifm as my file manager a couple of months ago.  But one  feature I was struggling with getting to work was image previews.  With  Überzug, and some scripts that the main developer recently posted on  Reddit, I was able to get image previews working on vifm! 

📰 REFERENCED: 

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fgithub.com%2Fseebye%2Fueberzug&amp;redir_token=BSESe_D7M7K4KJuaOfKVgxotQWR8MTU1MzY0MTU2OEAxNTUzNTU1MTY4&amp;v=qgxsduCO1pE&amp;event=video_description" target="_blank">https://github.com/seebye/ueberzug

<a href="https://www.youtube.com/watch?v=Bocb8diNJA8">https://www.youtube.com/watch?v=Bocb8...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.reddit.com%2Fr%2Flinux%2Fcomments%2Faviu08%2Fueberzug_v1810_released%2F&amp;redir_token=BSESe_D7M7K4KJuaOfKVgxotQWR8MTU1MzY0MTU2OEAxNTUzNTU1MTY4&amp;v=qgxsduCO1pE&amp;event=video_description" target="_blank">https://www.reddit.com/r/linux/commen...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fgitlab.com%2Fdwt1%2Fdotfiles%2Ftree%2Fmaster%2F.config%2Fvifm&amp;redir_token=BSESe_D7M7K4KJuaOfKVgxotQWR8MTU1MzY0MTU2OEAxNTUzNTU1MTY4&amp;v=qgxsduCO1pE&amp;event=video_description" target="_blank">https://gitlab.com/dwt1/dotfiles/tree...
