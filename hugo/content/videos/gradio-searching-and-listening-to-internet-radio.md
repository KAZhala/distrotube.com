---
title: "Gradio - Searching and Listening to Internet Radio Stations"
image: images/thumbs/0052.jpg
date: Thu, 07 Dec 2017 14:43:50 +0000
author: Derek Taylor
tags: ["GUI Apps", "gradio"]
---

#### VIDEO

{{< amazon src="Gradio+-+Searching+and+Listening+to+Internet+Radio+Stations.mp4" >}}  
&nbsp;

#### SHOW NOTES

Gradio is a GTK3 application that is used to search and listen to Internet radio stations. Easy-to-use, supports close-to-tray and supports notifications. <a href="https://github.com/haecker-felix/gradio">https://github.com/haecker-felix/gradio</a>
