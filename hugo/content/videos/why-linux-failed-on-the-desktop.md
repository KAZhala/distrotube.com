---
title: "Why Linux Failed On The Desktop"
image: images/thumbs/0236.jpg
date: Tue, 21 Aug 2018 19:10:52 +0000
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

{{< amazon src="Why+Linux+Failed+On+The+Desktop.mp4" >}}
&nbsp;

#### SHOW NOTES

Is this going to be the "Year of Linux on the Desktop"?  We've asked this question every year for about 15 years now.  It's never happened and it's not likely to happen.  I give five reasons why I think Linux failed on the desktop.
