---
title: "Obscure Window Manager Project - Xmonad"
image: images/thumbs/0138.jpg
date: Sat, 10 Mar 2018 21:28:37 +0000
author: Derek Taylor
tags: ["xmonad", ""]
---

#### VIDEO

{{< amazon src="Obscure+Window+Manager+Project+-+Xmonad.mp4" >}}  
&nbsp;

#### SHOW NOTES

This is number 7 of 12 obscure window managers I'm taking a look at in this series. That window manager is xmonad, a tiling window manager written in Haskell. Lightweight, powerful and a bit of a pain to configure. Being that I'm not a Haskell programmer, I do a very quick overview of xmonad in this video. <a href="http://xmonad.org/">http://xmonad.org/</a>
