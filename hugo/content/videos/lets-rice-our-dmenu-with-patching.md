---
title: "Let's Rice Our Dmenu With Patching"
image: images/thumbs/0557.jpg
date: 2020-02-28T12:22:40+06:00
author: Derek Taylor
tags: ["dmenu", ""]
---

#### VIDEO

{{< amazon src="Let's+Rice+Our+Dmenu+With+Patching.mp4" >}}
&nbsp;

#### SHOW NOTES

AndroI felt like ricing my desktop a bit today.  So I fiddled with new wallpapers and adjusted some color schemes.  But since dmenu figures so prominently in my workflow, I really wanted to add some bling to dmenu.  So I'm ditching my rather vanilla dmenu and building a new one...with a bunch of cool patches from the Suckless.org website.id-x86 is a project to port Android to the x86 platform.  So you can install Android-x86 to your traditional desktops and laptops.  This is an open source project licensed under Apache Public License 2.0.

REFERENCED:
+ https://suckless.org/ - Suckless.org