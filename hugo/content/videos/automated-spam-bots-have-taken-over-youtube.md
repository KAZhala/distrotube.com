---
title: "Automated Spam Bots Have Completely Taken Over YouTube"
image: images/thumbs/0626.jpg
date: 2020-05-19T12:23:40+06:00
author: Derek Taylor
tags: ["YouTube", ""]
---

#### VIDEO

{{< amazon src="Automated+Spam+Bots+Have+Completely+Taken+Over+YouTube.mp4" >}}
&nbsp;

#### SHOW NOTES

Awesome content!  You deserve more subscribers!  Wanna be friends?  You have seen this automated bot spam from Tom, Tim, Todd, TheComicalCanadian, Tyler, Call, Vakzy and Logan.  The names keep changing.  YouTubers are mad about all this spam.  Some even claim to have been hacked because they replied to the bot messages.  These kinds of claims have scared many content creators and content consumers on the platform.