---
title: "After Two Weeks With AwesomeWM..."
image: images/thumbs/0400.jpg
date: Wed, 22 May 2019 14:46:03 +0000
author: Derek Taylor
tags: ["tiling window managers", "awesome"]
---

#### VIDEO

{{< amazon src="After+Two+Weeks+With+AwesomeWM...-eUV94f_uMFI.mp4" >}}
&nbsp;

#### SHOW NOTES

After two weeks on AwesomeWM, how a I getting along with it? What have I changed, especially now that I am using the ErgoDox EZ keyboard. Also, I've been incorporating a bit more dmenu in my life.


&nbsp;
#### REFERENCED:
+ https://awesomewm.org/
