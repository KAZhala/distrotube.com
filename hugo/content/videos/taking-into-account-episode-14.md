---
title: "Taking Into Account, Ep 14 - IBM, Red Hat, Ikey Doherty, Thelio, Steam"
image: images/thumbs/0302.jpg
date: Fri, 02 Nov 2018 00:09:14 +0000
author: Derek Taylor
tags: ["Taking Into Account", ""]
---

#### VIDEO

{{< amazon src="Taking+Into+Account%2C+Ep+14+-+IBM%2C+Red+Hat%2C+Ikey+Doherty%2C+Thelio%2C+Steam.mp4" >}}
&nbsp;

#### SHOW NOTES

Today's live event will be an unusual one: Taking Into Account LIVE!  

 <a href="https://www.youtube.com/watch?v=lhQbxvAVcsQ&amp;t=402s">6:42 IBM buys Red Hat. What does this mean for Red Hat, Canonical, the kernel? 

<a href="https://www.youtube.com/watch?v=lhQbxvAVcsQ&amp;t=1139s">18:59 Ikey Doherty officially announces that he is leaving Solus development. 

<a href="https://www.youtube.com/watch?v=lhQbxvAVcsQ&amp;t=1698s">28:18 System76 unveils its new Thelio computers. 

<a href="https://www.youtube.com/watch?v=lhQbxvAVcsQ&amp;t=2546s">42:26 Steam for Linux now supports over 2,600 Windows games...for Linux! 

<a href="https://www.youtube.com/watch?v=lhQbxvAVcsQ&amp;t=2964s">49:24 Eight spooky terminal commands. 

<a href="https://www.youtube.com/watch?v=lhQbxvAVcsQ&amp;t=3491s">58:11 I read a viewer question about polkit. 

REFERENCED: 

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fgizmodo.com%2Fwhat-will-become-of-linux-giant-red-hat-now-that-it-sol-1830074632&amp;v=lhQbxvAVcsQ&amp;event=video_description&amp;redir_token=skA9Pe6NOCVAITDyCFtP_lgE_N18MTU1MzQ3MjU1MEAxNTUzMzg2MTUw" target="_blank">https://gizmodo.com/what-will-become-...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.omgubuntu.co.uk%2F2018%2F10%2Fmark-shuttleworth-thinks-ibm-buying-red-hat-is-good-news-for-ubuntu&amp;v=lhQbxvAVcsQ&amp;event=video_description&amp;redir_token=skA9Pe6NOCVAITDyCFtP_lgE_N18MTU1MzQ3MjU1MEAxNTUzMzg2MTUw" target="_blank">https://www.omgubuntu.co.uk/2018/10/m...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.theregister.co.uk%2F2018%2F11%2F02%2Frhel_deprecates_kde%2F&amp;v=lhQbxvAVcsQ&amp;event=video_description&amp;redir_token=skA9Pe6NOCVAITDyCFtP_lgE_N18MTU1MzQ3MjU1MEAxNTUzMzg2MTUw" target="_blank">https://www.theregister.co.uk/2018/11...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.phoronix.com%2Fscan.php%3Fpage%3Dnews_item%26px%3DSolus-Open-Letter&amp;v=lhQbxvAVcsQ&amp;event=video_description&amp;redir_token=skA9Pe6NOCVAITDyCFtP_lgE_N18MTU1MzQ3MjU1MEAxNTUzMzg2MTUw" target="_blank">https://www.phoronix.com/scan.php?pag...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fsystem76.com%2Fdesktops&amp;v=lhQbxvAVcsQ&amp;event=video_description&amp;redir_token=skA9Pe6NOCVAITDyCFtP_lgE_N18MTU1MzQ3MjU1MEAxNTUzMzg2MTUw" target="_blank">https://system76.com/desktops

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fliliputing.com%2F2018%2F10%2Fsteam-for-linux-now-supports-more-than-2600-windows-games.html&amp;v=lhQbxvAVcsQ&amp;event=video_description&amp;redir_token=skA9Pe6NOCVAITDyCFtP_lgE_N18MTU1MzQ3MjU1MEAxNTUzMzg2MTUw" target="_blank">https://liliputing.com/2018/10/steam-...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.protondb.com%2F&amp;v=lhQbxvAVcsQ&amp;event=video_description&amp;redir_token=skA9Pe6NOCVAITDyCFtP_lgE_N18MTU1MzQ3MjU1MEAxNTUzMzg2MTUw" target="_blank">https://www.protondb.com/

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fopensource.com%2Farticle%2F18%2F10%2Fspookier-side-unix-linux&amp;v=lhQbxvAVcsQ&amp;event=video_description&amp;redir_token=skA9Pe6NOCVAITDyCFtP_lgE_N18MTU1MzQ3MjU1MEAxNTUzMzg2MTUw" target="_blank">https://opensource.com/article/18/10/...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwiki.archlinux.org%2Findex.php%2FPolkit&amp;v=lhQbxvAVcsQ&amp;event=video_description&amp;redir_token=skA9Pe6NOCVAITDyCFtP_lgE_N18MTU1MzQ3MjU1MEAxNTUzMzg2MTUw" target="_blank">https://wiki.archlinux.org/index.php/...
