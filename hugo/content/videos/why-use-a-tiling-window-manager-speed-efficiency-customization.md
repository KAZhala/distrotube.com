---
title: "Why Use A Tiling Window Manager? Speed, Efficiency and Customization!"
image: images/thumbs/0330.jpg
date: Mon, 07 Jan 2019 20:00:01 +0000
author: Derek Taylor
tags: ["tiling window managers", ""]
---

#### VIDEO

{{< amazon src="Why+Use+A+Tiling+Window+Manager+Speed%2C+Efficiency+and+Customization.mp4" >}}
&nbsp;

#### SHOW NOTES

One of the questions that I've been getting asked over and over again--why bother with a tiling window manager? Let's discuss!
