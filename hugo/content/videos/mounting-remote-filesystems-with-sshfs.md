---
title: "Mounting Remote Filesystems With SSHFS"
image: images/thumbs/0602.jpg
date: 2020-04-20T12:23:40+06:00
author: Derek Taylor
tags: ["terminal", "command line", "ssh"]
---

#### VIDEO

{{< amazon src="Mounting+Remote+Filesystems+With+SSHFS.mp4" >}}
&nbsp;

#### SHOW NOTES

SSHFS allows you to mount a remote filesystem and is very simple to use.  SSHFS is shipped by all major Linux distributions and requires not setup.  In this video, I demonstrate mounting the filesystem on one of my laptops to my desktop computer.

REFERENCED:
+ https://github.com/libfuse/sshfs - SSHFS GitHub
