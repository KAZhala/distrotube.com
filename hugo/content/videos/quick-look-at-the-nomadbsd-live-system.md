---
title: "Quick Look At The NomadBSD Live System"
image: images/thumbs/0467.jpg
date: Sat, 23 Nov 2019 00:20:00 +0000
author: Derek Taylor
tags: ["Distro Reviews", "NomadBSD"]
---

#### VIDEO

{{< amazon src="Quick+Look+At+The+NomadBSD+Live+System.mp4" >}}
&nbsp;

#### SHOW NOTES

I'm going to take a quick look at NomadBSD 1.3 RC1, which is a live system built upon FreeBSD. It is designed to be used for typical desktop usage although it would also work fine as a system rescue distro. 

#### REFERENCED:
+ ► http://nomadbsd.org/
