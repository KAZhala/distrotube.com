---
title: 'Elementary OS "Loki" Install & Review'
image: images/thumbs/0098.jpg
date: Thu, 25 Jan 2018 01:26:30 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Elementary OS"]
---

#### VIDEO

{{< amazon src="Elementary+OS+Loki+Install+%26+Review.mp4" >}}  
&nbsp;

#### SHOW NOTES

In this quick install and review video, I take a look at Elementary OS. Elementary is an Ubuntu-based distro that features its own custom desktop (Pantheon) and a bunch of custom-built apps. It is attractive and easy-to-use---perfect for the new-to-Linux user! <a href="https://elementary.io/">https://elementary.io/</a>
