---
title: "Is LBRY a Viable Alternative to YouTube?"
image: images/thumbs/0510.jpg
date: 2020-01-09T12:22:40+06:00
author: Derek Taylor
tags: ["YouTube", "LBRY"]
---

#### VIDEO

{{< amazon src="Is+LBRY+a+Viable+Alternative+to+YouTube.mp4" >}}
&nbsp;

#### SHOW NOTES

You guys have been asking for alternative platforms to watch my videos on.  One of the ones that you guys have recommended is LBRY.  So I started syncing my YouTube channel to LBRY a few days ago.  Here are some of my initial thoughts.

REFERENCED:
+ ► https://lbry.tv/@DistroTube:2
