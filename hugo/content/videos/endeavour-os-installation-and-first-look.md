---
title: "Endeavour OS - Installation and First Look"
image: images/thumbs/0428.jpg
date: Tue, 06 Aug 2019 23:14:00 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Endeavour OS"]
---

#### VIDEO

{{< amazon src="Endeavour+OS+Installation+and+First+Look.mp4" >}}
&nbsp;

#### SHOW NOTES

In this video, I'm going to take a quick look at Endeavour OS, an Arch-based Linux distro that is a fork of the now dead Antergos project. It uses the Calamares installer, the XFCE desktop and promises a friendly community.


&nbsp;
#### REFERENCED:
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fendeavouros.com%2F&amp;event=video_description&amp;v=r4h67_frm6E&amp;redir_token=W6uMgc6xAOV_JyXS3mrSEx0GTZh8MTU3NzQ4ODQ4M0AxNTc3NDAyMDgz" target="_blank" rel="nofollow noopener noreferrer">https://endeavouros.com/
