---
title: "YouTube Sent Me A Termination Letter. New Monetization Policy."
image: images/thumbs/0091.jpg
date: Mon, 18 Jan 2018 01:17:54 +0000
author: Derek Taylor
tags: ["YouTube"]
---

#### VIDEO

{{< amazon src="YouTube+Sent+Me+A+Termination+Letter.+New+Monetization+Policy..mp4" >}}
&nbsp;

#### SHOW NOTES

YouTube announced a couple of days ago that they are making the requirements to join the YouTube Partner Program (YPP) a bit stricter.  YouTube will now require a miminum of 4000 hours of watch time in the last 12 months and at least 1000 subscribers in order for a channel to be eligible for monetization.  How big a blow is this to smaller YouTube channels (such as my own)?

YouTube announcement:
https://youtube-creators.googleblog.com/2018/01/additional-changes-to-youtube-partner.html
Youtube advertiser-friendly content guidelines:
https://support.google.com/youtube/answer/6162278?hl=en

Article referenced:
https://www.forbes.com/sites/erikkain/2018/01/18/youtube-is-demonetizing-small-channels-and-why-thats-a-good-thing/
