---
title: "rTorrent Is A Great Terminal Based BitTorrent Client"
image: images/thumbs/0605.jpg
date: 2020-04-24T12:23:40+06:00
author: Derek Taylor
tags: ["terminal", ""]
---

#### VIDEO

{{< amazon src="rTorrent+Is+A+Great+Terminal-Based+BitTorrent+Client.mp4" >}}
&nbsp;

#### SHOW NOTES

rTorrent is a terminal-based bitTorrent client that is simple to configure and use.  It is free and open source software and should be in every Linux distribution's repos.  Want a sexier rTorrent?  Try out rtorrent-ps!

REFERENCED:
+ https://github.com/rakshasa/rtorrent
+ https://github.com/pyroscope/rtorrent-ps
