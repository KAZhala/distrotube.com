---
title: "These Things Are Preventing Your YouTube Channel From Succeeding"
image: images/thumbs/0618.jpg
date: 2020-05-09T12:23:40+06:00
author: Derek Taylor
tags: ["YouTube", ""]
---

#### VIDEO

{{< amazon src="These+Things+Are+Preventing+Your+YouTube+Channel+From+Succeeding.mp4" >}}
&nbsp;

#### SHOW NOTES

Are you a YouTube creator?  Is your channel just not taking off like you had hoped?  In this video, I will discuss some things that you should be doing and some things that you should not be doing regarding your YouTube channel. 

Recently, fellow Linux YouTuber 'baby WOGUE' posted this video: 
https://www.youtube.com/watch?v=c8R-Cz-9v5c 

In this video, baby WOGUE laments the fact that his channel is not making the kind of revenue that it should.  So I take a look at baby WOGUE's channel and offer some constructive criticism that hopefully will help him and other YouTubers that are not achieving their desired results.

If you are looking for some great Linux content and are not subscribed to baby WOGUE, be sure to check out his channel and subscribe:
https://www.youtube.com/channel/UCZWadyLVO4ZnMgLrRVtS6VA