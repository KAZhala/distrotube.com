---
title: "Panasonic Lumix G7 Plus Accessories"
image: images/thumbs/0269.jpg
date: Mon, 27 Aug 2018 18:15:51 +0000
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

{{< amazon src="Panasonic+Lumix+G7+Plus+Accessories.mp4" >}}
&nbsp;

#### SHOW NOTES

A quick look at my new camera--the Panasonic Lumix G7. 

LINKS TO WHAT I BOUGHT: 

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=GUMK7aEMWTogtQh4o0BoLjbbmoh8MTU1MzQ1MTM3MEAxNTUzMzY0OTcw&amp;q=https%3A%2F%2Fwww.amazon.com%2Fgp%2Fproduct%2FB00Y111XC8%2Fref%3Doh_aui_detailpage_o01_s00%3Fie%3DUTF8%26psc%3D1&amp;event=video_description&amp;v=3mu_Ffrvayg" target="_blank">https://www.amazon.com/gp/product/B00...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=GUMK7aEMWTogtQh4o0BoLjbbmoh8MTU1MzQ1MTM3MEAxNTUzMzY0OTcw&amp;q=https%3A%2F%2Fwww.amazon.com%2Fgp%2Fproduct%2FB014RD6RC0%2Fref%3Doh_aui_detailpage_o00_s01%3Fie%3DUTF8%26psc%3D1c&amp;event=video_description&amp;v=3mu_Ffrvayg" target="_blank">https://www.amazon.com/gp/product/B01...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=GUMK7aEMWTogtQh4o0BoLjbbmoh8MTU1MzQ1MTM3MEAxNTUzMzY0OTcw&amp;q=https%3A%2F%2Fwww.amazon.com%2Fgp%2Fproduct%2FB074W8975Z%2Fref%3Doh_aui_detailpage_o00_s00%3Fie%3DUTF8%26psc%3D1&amp;event=video_description&amp;v=3mu_Ffrvayg" target="_blank">https://www.amazon.com/gp/product/B07...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=GUMK7aEMWTogtQh4o0BoLjbbmoh8MTU1MzQ1MTM3MEAxNTUzMzY0OTcw&amp;q=https%3A%2F%2Fwww.amazon.com%2Fgp%2Fproduct%2FB014RD6RC0%2Fref%3Doh_aui_detailpage_o00_s01%3Fie%3DUTF8%26psc%3D1&amp;event=video_description&amp;v=3mu_Ffrvayg" target="_blank">https://www.amazon.com/gp/product/B01...
