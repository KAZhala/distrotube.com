---
title: "90 Minute Q & A - DT LIVE"
image: images/thumbs/0587.jpg
date: 2020-04-03T12:23:40+06:00
author: Derek Taylor
tags: ["Live Stream", ""]
---

#### VIDEO

{{< amazon src="90+Minute+Q+%26+A+-+DT+LIVE-13pTBll1mDM.mp4" >}}
&nbsp;

#### SHOW NOTES

You got questions.  DT got answers.