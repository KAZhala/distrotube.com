---
title: "Three Weeks With i3 - Time For A New Challenge - SUCKLESS!"
image: images/thumbs/0339.jpg
date: Mon, 21 Jan 2019 22:34:33 +0000
author: Derek Taylor
tags: ["tiling window managers", "i3wm", "dwm"]
---

#### VIDEO

{{< amazon src="Three+Weeks+With+i3+Time+For+A+New+Challenge+SUCKLESS.mp4" >}}
&nbsp;

#### SHOW NOTES

It's been three weeks since I switched from qtile to i3 for my window  manager.  And i3 has been great.  I've gotten used to the workflow and  have completely forgotten that I'm using a new-to-me window manager.  So  it's time for a new challenge.  I'm switching to dwm, a dynamic tiling  window manager from the good folks over at suckless.org.   But I'm not  just switching to dwm; I'm going "full suckless." 

📰 REFERENCED: 

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fsuckless.org%2F&amp;redir_token=_clHbcOVk7LwQxaJDLk2VXOgYMp8MTU1MzYzOTY3OUAxNTUzNTUzMjc5&amp;event=video_description&amp;v=JRRAZrALZpY" target="_blank">https://suckless.org/
