---
title: "FOSS Gaming With The Radeon VII - 0 A.D."
image: images/thumbs/0378.jpg
date: Mon, 08 Apr 2019 02:46:25 +0000
author: Derek Taylor
tags: ["Gaming", "Live Stream"]
---

#### VIDEO

{{< amazon src="FOSS+Gaming+With+The+Radeon+VII+-+0+A.D.-pxSNPmHJcM0.mp4" >}}
&nbsp;

#### SHOW NOTES

Playing one of the truly impressive free and open source games that we  have--0 A.D.  It is a real-time strategy game that is reminiscent of Age  of Empires. <a rel="noreferrer noopener" href="https://www.youtube.com/redirect?event=video_description&amp;v=pxSNPmHJcM0&amp;q=https%3A%2F%2Fplay0ad.com%2F&amp;redir_token=MDNsxHgQtlW0RnbS1dn_PU1MCfx8MTU1NzE5NzE4OEAxNTU3MTEwNzg4" target="_blank">https://play0ad.com/
