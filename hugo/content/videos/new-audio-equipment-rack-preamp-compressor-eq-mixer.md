---
title: "New Audio Equipment - Rack, Preamp, Compressor, EQ, Mixer"
image: images/thumbs/0431.jpg
date: Tue, 13 Aug 2019 23:19:00 +0000
author: Derek Taylor
tags: ["Audio", ""]
---

#### VIDEO

{{< amazon src="New+Audio+Equipment+Rack+Preamp+Compressor+EQ+Mixer.mp4" >}}
&nbsp;

#### SHOW NOTES

I bought a new audio rack and some equipment to mount in it, including a compressor, an EQ, and a mixer. I want to get more into hardware compression and EQ rather than relying on software to do the job. Just getting this stuff plugged in and seeing how it sounds. WARNING: I'm a complete noob to audio equipment.


&nbsp;
#### REFERENCED:
+ StarTech.com 12U Open Frame Network Rack
+ Nady PRA-8 Microphone Preamplifier
+ dbx 166xs Professional Audio Compressor/Limiter/Gate
+ dbx 231s Dual Channel 31-Band Equalizer 
+ Furman M-8x2 Merit Series 8 Outlet Power Conditioner
+ Mackie PROFX12V2 12-Channel Compact Mixer
