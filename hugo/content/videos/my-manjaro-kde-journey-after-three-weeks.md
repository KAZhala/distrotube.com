---
title: "My Manjaro KDE Journey, After Three Weeks"
image: images/thumbs/0092.jpg
date: Mon, 19 Jan 2018 00:55:50 +0000
author: Derek Taylor
tags: ["Manjaro", "KDE"]
---

#### VIDEO

{{< amazon src="My+Manjaro+KDE+Journey%2C+After+Three+Weeks.mp4" >}}  
&nbsp;

#### SHOW NOTES

How's Manjaro KDE been treating me?  Find out in this quick video update.  I discuss what I love about Manjaro.  I also discuss what I hate about KDE and why I probably need to switch to another desktop environment soon.
