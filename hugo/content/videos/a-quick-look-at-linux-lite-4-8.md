---
title: "A Quick Look At Linux Lite 4.8"
image: images/thumbs/0519.jpg
date: 2020-01-19T12:22:40+06:00
author: Derek Taylor
tags: ["Distro Reviews", "Linux Lite"]
---

#### VIDEO

{{< amazon src="A+Quick+Look+At+Linux+Lite+4-8.mp4" >}}
&nbsp;

#### SHOW NOTES

I'm taking the recently released Linux Lite 4.8 for spin.  This is an Ubuntu-based distro that uses the Xfce desktop environment.  It is light and fast.  And this version is aimed at winning over those folks that are still using Windows 7.

REFERENCED:
+ https://www.linuxliteos.com/