---
title: "A Look at How I Setup Virtualbox For Testing Linux Distros"
image: images/thumbs/0068.jpg
date: Tue, 19 Dec 2017 15:27:40 +0000
author: Derek Taylor
tags: ["Virtualbox"]
---

#### VIDEO

{{< amazon src="A+Look+at+How+I+Setup+Virtualbox+For+Testing+Linux+Distros.mp4" >}}  
&nbsp;

#### SHOW NOTES

I discuss how I use Virtualbox to install and test out Linux distros. I run through some of the settings I use and some of the features of Virtualbox. <a href="https://www.virtualbox.org/">https://www.virtualbox.org/</a>
