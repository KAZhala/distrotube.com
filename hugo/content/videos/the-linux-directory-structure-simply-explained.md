---
title: "The Linux Directory Structure Simply Explained"
image: images/thumbs/0198.jpg
date: Sun, 06 May 2018 23:00:42 +0000
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

{{< amazon src="The+Linux+Directory+Structure+Simply+Explained.mp4" >}}
&nbsp;

#### SHOW NOTES

In this video, I take a quick look at the Linux filesystem and how it is structured.
