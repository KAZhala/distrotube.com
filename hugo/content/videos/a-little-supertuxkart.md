---
title: "A Little SuperTuxKart With a Bit of Talk About Linux Gaming of the Past"
image: images/thumbs/0059.jpg
date: Wed, 13 Dec 2017 15:15:30 +0000
author: Derek Taylor
tags: ["Gaming"]
---

#### VIDEO

{{< amazon src="A+Little+SuperTuxKart+With+a+Bit+of+Talk+About+Linux+Gaming+of+the+Past.mp4" >}}  
&nbsp;

#### SHOW NOTES

A short and somewhat pointless video of me playing the latest SuperTuxKart, and doing a rather poor job of playing the game. I also spout some random gibberish about the state of Linux gaming in the past. <a href="https://supertuxkart.net/Main_Page">https://supertuxkart.net/Main_Page</a>
