---
title: "A Quick Look at the Official Release of MX Linux 17"
image: images/thumbs/0064.jpg
date: Sun, 17 Dec 2017 15:22:13 +0000
author: Derek Taylor
tags: ["Distro Reviews", "MX Linux", "XFCE"]
---

#### VIDEO

{{< amazon src="A+Quick+Look+at+the+Official+Release+of+MX+Linux+17.mp4" >}}  
&nbsp;

#### SHOW NOTES

About a month ago, I took a look the beta version of MX Linux 17 and loved it. In this video, I take a brief look at the official release of MX 17. <a href="https://mxlinux.org/">https://mxlinux.org/</a>
