---
title: "My Thoughts After One Year With The ErgoDox EZ"
image: images/thumbs/0585.jpg
date: 2020-04-01T12:23:40+06:00
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

{{< amazon src="My+Thoughts+After+One+Year+With+The+ErgoDox+EZ.mp4" >}}
&nbsp;

#### SHOW NOTES

In May of 2019, I made a video about my purchase of the ErgoDox EZ programmable keyboard, and since that video one of the most common questions I get asked is "How are you liking the ErgoDox?"  Here are my thoughts after nearly a year of using this keyboard.

REFERENCED:
+ https://ergodox-ez.com/