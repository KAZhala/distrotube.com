---
title: "The Surf Browser - Bookmarks, Stylesheets and Tabs"
image: images/thumbs/0353.jpg
date: Mon, 18 Feb 2019 22:57:37 +0000
author: Derek Taylor
tags: ["suckless", "surf"]
---

#### VIDEO

{{< amazon src="The+Surf+Browser+Bookmarks+Stylesheets+and+Tabs.mp4" >}}
&nbsp;

#### SHOW NOTES

Let's start hacking on the Surf browser (from the Suckless Project) to make it a bit more user-friendly. 

📰 REFERENCED: 

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?event=video_description&amp;v=Dlcj0onPrU8&amp;q=https%3A%2F%2Fsurf.suckless.org%2F&amp;redir_token=3MsxehSXs8mg4NkWZ_hKnDKdDfN8MTU1MzY0MTA2MkAxNTUzNTU0NjYy" target="_blank">https://surf.suckless.org/

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?event=video_description&amp;v=Dlcj0onPrU8&amp;q=https%3A%2F%2Ftools.suckless.org%2Ftabbed%2F&amp;redir_token=3MsxehSXs8mg4NkWZ_hKnDKdDfN8MTU1MzY0MTA2MkAxNTUzNTU0NjYy" target="_blank">https://tools.suckless.org/tabbed/

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?event=video_description&amp;v=Dlcj0onPrU8&amp;q=http%3A%2F%2Ftroubleshooters.com%2Flinux%2Fsurf.htm&amp;redir_token=3MsxehSXs8mg4NkWZ_hKnDKdDfN8MTU1MzY0MTA2MkAxNTUzNTU0NjYy" target="_blank">http://troubleshooters.com/linux/surf...
