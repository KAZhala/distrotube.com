---
title: "Ubuntu 17.10 - Install and Review - Big Changes For Ubuntu?"
image: images/thumbs/0004.jpg
date: Mon, 09 Oct 2017 01:41:29 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Ubuntu", "GNOME"]
---

#### VIDEO

{{< amazon src="Ubuntu+17.10+-+Install+and+Review+-+Big+Changes+For+Ubuntu.mp4" >}}  
&nbsp;

#### SHOW NOTES

Today I install and review Ubuntu 17.10...well, the Beta version of 17.10. We still are 10 days away from the official release of Ubuntu 17.10 but we are so close that little (probably nothing) will change between now and the release date. Let's take a look!
