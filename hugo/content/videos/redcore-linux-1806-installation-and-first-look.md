---
title: "Redcore Linux 1806 Installation and First Look"
image: images/thumbs/0239.jpg
date: Mon, 02 Jul 2018 00:18:01 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Redcore Linux"]
---

#### VIDEO

{{< amazon src="Redcore+Linux+1806+Installation+and+First+Look.mp4" >}}
&nbsp;

#### SHOW NOTES

Today, I'm taking a look the recently released Redcore Linux 1806. Redcore is a Gentoo-based Linux distro that aims to be easy-to-install and easy-to-use. It sports the LXQt desktop environment by default.
