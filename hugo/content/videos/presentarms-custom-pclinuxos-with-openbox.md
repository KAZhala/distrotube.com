---
title: "Presentarms Custom PCLinuxOS With Openbox"
image: images/thumbs/0252.jpg
date: Tue, 31 Jul 2018 22:26:47 +0000
author: Derek Taylor
tags: ["PCLinuxOS", "Openbox"]
---

#### VIDEO

{{< amazon src="Presentarms+Custom+PCLinuxOS+With+Openbox.mp4" >}}
&nbsp;

#### SHOW NOTES

Tonight, I'm going to take a look at presentarms ( https://www.youtube.com/channel/UC7ub6cQ9iD1hyBP1onI5COg ) custom PCLinuxOS Openbox edition using my personal Openbox configs ( https://gitlab.com/dwt1 ). 
Download PCLinuxOS Trinity: http://trinity.mypclinuxos.com/
