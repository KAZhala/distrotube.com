---
title: "The Openbox Window Manager - Install & Setup"
image: images/thumbs/0056.jpg
date: Sun, 10 Dec 2017 14:50:27 +0000
author: Derek Taylor
tags: ["Openbox"]
---

#### VIDEO

{{< amazon src="The+Openbox+Window+Manager+-+Install+%26+Setup.mp4" >}}  
&nbsp;

#### SHOW NOTES

A quick look at the Openbox window manager, a very lightweight and customizable window manager that is perfect for older computers or those that want SPEED! I discuss how to install Openbox and how to configure it to look like a proper desktop. <a href="http://openbox.org/wiki/Main_Page">http://openbox.org/wiki/Main_Page</a>
