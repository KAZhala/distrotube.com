---
title: "Top Five Linux Distros of 2017"
image: images/thumbs/0072.jpg
date: Sat, 23 Dec 2017 15:52:26 +0000
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

{{< amazon src="Top+Five+Linux+Distros+of+2017.mp4" >}}  
&nbsp;

#### SHOW NOTES

A look at my top five Linux distros of 2017. These are the top five distros that I reviewed in 2017 that impressed me the most. Happy Holidays!
