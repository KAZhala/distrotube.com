---
title: "Big YouTubers And Linux - So Little Coverage And It's Usually Bad!"
image: images/thumbs/0372.jpg
date: Thu, 28 Mar 2019 01:17:01 +0000
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

{{< amazon src="Big+YouTubers+And+Linux+So+Little+Coverage+And+Usually+Bad.mp4" >}}
&nbsp;

#### SHOW NOTES

WARNING: Rant incoming! To quote the Allman Brothers, "Lord, I was born a ramblin' man…" Why do the really big gaming YouTubers and tech YouTubers rarely mention Linux, and when they do cover Linux, why do they struggle so much to accurately depict Linux? 
