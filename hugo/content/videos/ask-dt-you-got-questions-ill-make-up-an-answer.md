---
title: "Ask DT! You Got Questions? I'll Make Up An Answer."
image: images/thumbs/0388.jpg
date: Sun, 21 Apr 2019 16:56:08 +0000
author: Derek Taylor
tags: ["Live Stream", ""]
---

#### VIDEO

{{< amazon src="Ask+DT!++You+Got+Questions++I'll+Make+Up+An+Answer.-OidW7bOZL38.mp4" >}}
&nbsp;

#### SHOW NOTES

Tonight's live stream will be a live Q&amp;A with you guys in the 
chat(s).  Ask a question via the YouTube chat or via the IRC channel.  
Join <a href="https://www.youtube.com/results?search_query=%23distrotube">#distrotube on Freenode for the IRC chat.
