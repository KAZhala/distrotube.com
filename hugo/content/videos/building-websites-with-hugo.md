---
title: "Building Websites With Hugo"
image: images/thumbs/0490.jpg
date: 2020-01-02T12:22:40+06:00
author: Derek Taylor
tags: ["Web Development", "Hugo"]
---

#### VIDEO

{{< amazon src="Building+Websites+With+Hugo.mp4" >}}
&nbsp;

#### SHOW NOTES

For the next 30- days, I'm giving myself a challenge--live in a tiling window manager and use only terminal-based applications (where possible). What will be the result of this experiment? 


So I have two problems that I have been needing to solve:


First, I have been needing to mirror my content somewhere other than YouTube.  I understand that many do no want to be forced to use YouTube to watch my content.  So I decided to take matters into my own hands and mirror my content on my own website.


Second, for years I've complained about the web being slow and bloated.  Part of the problem is that most websites today are dynamic, using things like MySQL databases and PHP, when really a static website would suffice.  So I'm creating my own website using Hugo, a static site generator.


REFERENCED:
+ ► https://gohugo.io/ - Hugo Documentation
+ ► http://www.distrotube.com/ - MAY NOT BE UP YET!/
