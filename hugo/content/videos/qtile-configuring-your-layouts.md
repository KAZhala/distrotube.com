---
title: "Qtile - Configuring Your Layouts"
image: images/thumbs/0482.jpg
date: Tue, 24 Dec 2019 00:20:00 +0000
author: Derek Taylor
tags: ["tiling window managers", "qtile"]
---

#### VIDEO

{{< amazon src="Qtile+Configuring+Your+Layouts.mp4" >}}
&nbsp;

#### SHOW NOTES

Are you wanting to try out the Qtile tiling window manager but you are not sure how to configure it to your liking?  In this video, I take you through  hacking on the default qtile config and adding and modifying the built-in layouts.

#### ERRATA: 
I was not paying close attention to the documentation when I added the new layouts to the default config file.  I should have added "layout.Floating()" and "layout.Bsp()" -- notice the capitalization.  I have the correct spelling in my personal config file.

#### REFERENCED:
+ ► http://www.qtile.org/ - Qtile Documentation
+ ► https://gitlab.com/dwt1/dotfiles/blob/master/.config/qtile/default_config.py - The Default Config
+ ► https://gitlab.com/dwt1/dotfiles/blob/master/.config/qtile/config.py - My Heavily Modified Config

