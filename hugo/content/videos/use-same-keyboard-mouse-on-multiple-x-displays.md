---
title: "Use The Same Keyboard And Mouse On Multiple X Displays"
image: images/thumbs/0621.jpg
date: 2020-05-13T12:23:40+06:00
author: Derek Taylor
tags: ["ssh", ""]
---

#### VIDEO

{{< amazon src="Use+The+Same+Keyboard+And+Mouse+On+Multiple+X+Displays.mp4" >}}
&nbsp;

#### SHOW NOTES

In this brief video, I introduce a nifty little utility that allows your keyboard and mouse on an X display to be used on another X display.   This is really nice if you have multiple machines in the same area, for example, a workstation and a laptop nearby.  The program is called is x2x.

REFERENCED:
+ https://github.com/dottedmag/x2x