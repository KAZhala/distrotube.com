---
title: "Generate Random Manpages To Learn Linux"
image: images/thumbs/0617.jpg
date: 2020-05-08T12:23:40+06:00
author: Derek Taylor
tags: ["terminal", "command line"]
---

#### VIDEO

{{< amazon src="Generate+Random+Manpages+To+Learn+Linux.mp4" >}}
&nbsp;

#### SHOW NOTES

In this lengthy rant video, I address a few questions that I've been receiving from viewers.  I discuss my thoughts on Linux and the free software movement becoming "centralized", questions I've gotten regarding my terminals aWant to learn Linux faster?  Why don't you write a script that randomly generates a manpage for you to read?   In fact, not only would reading random manpages help your Linux education, but writing the script to generate a random manpage would be fun and educational.  

REFERENCED:
+ https://www.kernel.org/doc/man-pages/nd window managers, as well as explaining why I've resorted to giving out RTFM's like it's Christmas!