---
title: "Mad About Snap? Remove Them. Use Flatpaks Instead."
image: images/thumbs/0606.jpg
date: 2020-04-25T12:23:40+06:00
author: Derek Taylor
tags: ["Ubuntu", ""]
---

#### VIDEO

{{< amazon src="Mad+About+Snaps+Remove+Them+Use+Flatpaks+Instead.mp4" >}}
&nbsp;

#### SHOW NOTES

So many people are complaining about Snaps on Ubuntu.  I personally don't understand all the whining when you can just not use Snaps.  You can even remove Snaps and install Flatpak on Ubuntu, if you prefer.  

REFERENCED:
+ https://snapcraft.io/
+ https://flathub.org/home
