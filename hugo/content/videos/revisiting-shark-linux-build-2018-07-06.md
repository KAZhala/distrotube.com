---
title: "Revisiting Shark Linux With Build 2018-07-06"
image: images/thumbs/0244.jpg
date: Mon, 16 Jul 2018 00:26:16 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Shark Linux"]
---

#### VIDEO

{{< amazon src="Revisiting+Shark+Linux+With+Build+2018-07-06.mp4" >}}
&nbsp;

#### SHOW NOTES

Today, I take another quick look at a current build of Shark Linux.  
