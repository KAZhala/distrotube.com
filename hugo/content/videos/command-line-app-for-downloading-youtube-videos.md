---
title: "Command Line App For Downloading YouTube Videos"
image: images/thumbs/0420.jpg
date: Sun, 14 Jul 2019 05:39:00 +0000
author: Derek Taylor
tags: ["command line", "terminal"]
---

#### VIDEO

{{< amazon src="Command+Line+App+For+Downloading+Youtube+Videos.mp4" >}}
&nbsp;

#### SHOW NOTES

Hate the YouTube platform? You don't have to be on YouTube to watch YouTube videos. The command line app youtube-dl (and the gui version youtube-dl-gui) can download single videos, playlists or entire channels to your local machine. The command line app is available on Windows, Mac and Linux. Besides Youtube, the command line app can be used to rip video from hundreds of other sites as well.


&nbsp;
#### REFERENCED:
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?v=fOjP-7-gI4Y&amp;event=video_description&amp;q=https%3A%2F%2Fytdl-org.github.io%2Fyoutube-dl%2Findex.html&amp;redir_token=Ra6yw165NPobThFM0AI-JyxsbPZ8MTU3NTY5NzE4MEAxNTc1NjEwNzgw" target="_blank" rel="nofollow noopener noreferrer">https://ytdl-org.github.io/youtube-dl...
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?v=fOjP-7-gI4Y&amp;event=video_description&amp;q=https%3A%2F%2Fgithub.com%2FMrS0m30n3%2Fyoutube-dl-gui&amp;redir_token=Ra6yw165NPobThFM0AI-JyxsbPZ8MTU3NTY5NzE4MEAxNTc1NjEwNzgw" target="_blank" rel="nofollow noopener noreferrer">https://github.com/MrS0m30n3/youtube-...
