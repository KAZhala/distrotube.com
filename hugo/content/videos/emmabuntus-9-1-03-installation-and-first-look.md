---
title: "Emmabuntüs 9-1.03 Installation and First Look"
image: images/thumbs/0295.jpg
date: Fri, 12 Oct 2018 23:53:09 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Emmabuntüs"]
---

#### VIDEO

{{< amazon src="Emmabuntus+9103+Installation+and+First+Look.mp4" >}}
&nbsp;

#### SHOW NOTES

Emmabuntüs is a Debian-based Linux distro that focuses on being light so
 that it can be used to refurbish older computers, especially computers 
that are being given to humanitarian organizations.

<a href="https://www.youtube.com/redirect?v=FX8TenuQVkM&amp;event=video_description&amp;redir_token=In5YI4X80Z3TGAi6w9_stEny6FN8MTU1MzQ3MTYxOUAxNTUzMzg1MjE5&amp;q=http%3A%2F%2Femmabuntus.org%2F" rel="noreferrer noopener" target="_blank">http://emmabuntus.org/
