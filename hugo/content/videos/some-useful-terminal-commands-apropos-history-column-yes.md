---
title: "Some Useful Terminal Commands - apropos, history, column, yes, fortune, cowsay"
image: images/thumbs/0111.jpg
date: Sat, 10 Feb 2018 01:50:19 +0000
author: Derek Taylor
tags: ["Terminal", "Command Line"]
---

#### VIDEO

{{< amazon src="Some+Useful+Terminal+Commands+-+apropos%2C+history%2C+column%2C+yes%2C+fortune%2C+cowsay.mp4" >}}  
&nbsp;

#### SHOW NOTES

A quick video of some useful terminal commands that are not as well known to the general public. Learning these extremely useful commands can make your time in the terminal more rewarding and improve your overall quality of life.
