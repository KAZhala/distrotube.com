---
title: "Slackware Current With Help From Serge"
image: images/thumbs/0251.jpg
date: Sun, 29 Jul 2018 22:23:39 +0000
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

{{< amazon src="Slackware+Current+With+Help+From+Serge.mp4" >}}
&nbsp;

#### SHOW NOTES

In this video, I install Slackware Current in a virtual machine with the help of Serge, who is one of the most active members of the Linux Youtube community. He is knowledgeable and is always offering support for Linux users in need. Thank you, Serge!
