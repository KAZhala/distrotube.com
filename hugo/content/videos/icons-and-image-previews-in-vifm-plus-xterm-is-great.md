---
title: "Icons And Image Previews In Vifm, Plus Xterm Is Great!"
image: images/thumbs/0335.jpg
date: Sun, 13 Jan 2019 20:13:30 +0000
author: Derek Taylor
tags: ["terminal", "vifm", "TUI Apps"]
---

#### VIDEO

{{< amazon src="Icons+And+Image+Previews+In+Vifm%2C+Plus+Xterm+Is+Great.mp4" >}}
&nbsp;

#### SHOW NOTES

To get icons in vifm, take a look at my vifmrc: <a href="https://www.youtube.com/redirect?q=https%3A%2F%2Fgitlab.com%2Fdwt1%2Fdotfiles%2Fblob%2Fmaster%2F.config%2Fvifm%2Fvifmrc&amp;v=rnMXH_K8hz4&amp;event=video_description&amp;redir_token=TvjqokEadccft1soqNyTWoSUMKh8MTU1MzU0NDgyOUAxNTUzNDU4NDI5" rel="noreferrer noopener" target="_blank">https://gitlab.com/dwt1/dotfiles/blob...
Look for the comment "GETTING ICONS TO DISPLAY IN VIFM"

To get image previews, you will have to have the right combination of 
font, terminal and config. Here is the official documentation: <a href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwiki.vifm.info%2Findex.php%2FHow_to_preview_images&amp;v=rnMXH_K8hz4&amp;event=video_description&amp;redir_token=TvjqokEadccft1soqNyTWoSUMKh8MTU1MzU0NDgyOUAxNTUzNDU4NDI5" rel="noreferrer noopener" target="_blank">https://wiki.vifm.info/index.php/How_...

If you want to use my imgt script, get it from my GitLab page.  Should work with UbuntuMono Nerd Font and xterm.

Good luck!
