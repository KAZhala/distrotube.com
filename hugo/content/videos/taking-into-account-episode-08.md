---
title: "Taking Into Account, Ep. 8 - Anticompetitive Microsoft, Social Spam, Linux Gaming, New Releases"
image: images/thumbs/0275.jpg
date: Thu, 13 Sep 2018 18:45:41 +0000
author: Derek Taylor
tags: ["Taking Into Account", ""]
---

#### VIDEO

{{< amazon src="Taking+Into+Account%2C+Ep.+8+-+Anticompetitive+Microsoft%2C+Social+Spam%2C+Linux+Gaming%2C+New+Releases.mp4" >}}
&nbsp;

#### SHOW NOTES

<a href="https://www.youtube.com/watch?v=hV1t_b4-UaM&amp;t=38s">0:38 Microsoft interrupts installation of Firefox/Chrome on recent Windows Insider build. 

<a href="https://www.youtube.com/watch?v=hV1t_b4-UaM&amp;t=491s">8:11 Facebook and Twitter have deleted billions of fake accounts in last few months. 

<a href="https://www.youtube.com/watch?v=hV1t_b4-UaM&amp;t=827s">13:47 What's true and what's false about gaming on Linux. 

<a href="https://www.youtube.com/watch?v=hV1t_b4-UaM&amp;t=1409s">23:29 A Windows user?  These headlines should have you running to Linux! 

<a href="https://www.youtube.com/watch?v=hV1t_b4-UaM&amp;t=1726s">28:46 Notable releases: Nano  3.0, Nextcloud 14 and Tor Browser 8.0 

<a href="https://www.youtube.com/watch?v=hV1t_b4-UaM&amp;t=1802s">30:02 I read a Reddit post...not a post to me, but about me! 

REFERENCED IN THE VIDEO: 

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=em0RP0A_YwVCOoLJhyB1I1vtDcd8MTU1MzQ1MzE5OUAxNTUzMzY2Nzk5&amp;q=https%3A%2F%2Fwww.ghacks.net%2F2018%2F09%2F12%2Fmicrosoft-intercepting-firefox-chrome-installation-on-windows-10%2F&amp;event=video_description&amp;v=hV1t_b4-UaM" target="_blank">https://www.ghacks.net/2018/09/12/mic...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=em0RP0A_YwVCOoLJhyB1I1vtDcd8MTU1MzQ1MzE5OUAxNTUzMzY2Nzk5&amp;q=https%3A%2F%2Fnews.softpedia.com%2Fnews%2F1-27-billion-fake-accounts-deleted-by-facebook-in-six-months-522590.shtml&amp;event=video_description&amp;v=hV1t_b4-UaM" target="_blank">https://news.softpedia.com/news/1-27-...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=em0RP0A_YwVCOoLJhyB1I1vtDcd8MTU1MzQ1MzE5OUAxNTUzMzY2Nzk5&amp;q=https%3A%2F%2Fnews.softpedia.com%2Fnews%2Fup-to-10-million-automated-accounts-detected-by-twitter-every-week-522597.shtml&amp;event=video_description&amp;v=hV1t_b4-UaM" target="_blank">https://news.softpedia.com/news/up-to...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=em0RP0A_YwVCOoLJhyB1I1vtDcd8MTU1MzQ1MzE5OUAxNTUzMzY2Nzk5&amp;q=https%3A%2F%2Fwww.forbes.com%2Fsites%2Fjasonevangelho%2F2018%2F09%2F04%2Fgaming-on-linux-2-ridiculous-myths-and-2-brutal-truths%2F%23487817675524&amp;event=video_description&amp;v=hV1t_b4-UaM" target="_blank">https://www.forbes.com/sites/jasoneva...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=em0RP0A_YwVCOoLJhyB1I1vtDcd8MTU1MzQ1MzE5OUAxNTUzMzY2Nzk5&amp;q=https%3A%2F%2Fspcr.netlify.com%2F&amp;event=video_description&amp;v=hV1t_b4-UaM" target="_blank">https://spcr.netlify.com/

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=em0RP0A_YwVCOoLJhyB1I1vtDcd8MTU1MzQ1MzE5OUAxNTUzMzY2Nzk5&amp;q=https%3A%2F%2Fwww.forbes.com%2Fsites%2Fjasonevangelho%2F2018%2F09%2F11%2F5-ridiculous-windows-10-headlines-that-will-have-you-running-to-linux&amp;event=video_description&amp;v=hV1t_b4-UaM" target="_blank">https://www.forbes.com/sites/jasoneva...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=em0RP0A_YwVCOoLJhyB1I1vtDcd8MTU1MzQ1MzE5OUAxNTUzMzY2Nzk5&amp;q=https%3A%2F%2Fitsfoss.com%2Fnano-3-release%2F&amp;event=video_description&amp;v=hV1t_b4-UaM" target="_blank">https://itsfoss.com/nano-3-release/

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=em0RP0A_YwVCOoLJhyB1I1vtDcd8MTU1MzQ1MzE5OUAxNTUzMzY2Nzk5&amp;q=https%3A%2F%2Fnextcloud.com%2Fblog%2Fnextcloud-14-focus-on-security-and-compliance%2F&amp;event=video_description&amp;v=hV1t_b4-UaM" target="_blank">https://nextcloud.com/blog/nextcloud-...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=em0RP0A_YwVCOoLJhyB1I1vtDcd8MTU1MzQ1MzE5OUAxNTUzMzY2Nzk5&amp;q=https%3A%2F%2Fblog.torproject.org%2Fnew-release-tor-browser-80&amp;event=video_description&amp;v=hV1t_b4-UaM" target="_blank">https://blog.torproject.org/new-relea..

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=em0RP0A_YwVCOoLJhyB1I1vtDcd8MTU1MzQ1MzE5OUAxNTUzMzY2Nzk5&amp;q=https%3A%2F%2Fblog.torproject.org%2Fnew-release-tor-browser-80&amp;event=video_description&amp;v=hV1t_b4-UaM" target="_blank">.<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=em0RP0A_YwVCOoLJhyB1I1vtDcd8MTU1MzQ1MzE5OUAxNTUzMzY2Nzk5&amp;q=https%3A%2F%2Fwww.reddit.com%2Fr%2Flinuxmasterrace%2Fcomments%2F9epr7h%2Fdistrotube_to_go_the_bryan_lunduke_way_will_lock%2F&amp;event=video_description&amp;v=hV1t_b4-UaM" target="_blank">https://www.reddit.com/r/linuxmasterr...
