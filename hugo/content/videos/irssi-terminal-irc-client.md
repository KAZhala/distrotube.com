---
title: "Irssi - Terminal IRC Client - Who Needs a GUI to Chat!"
image: images/thumbs/0022.jpg
date: Tue, 24 Oct 2017 02:24:36 +0000
author: Derek Taylor
tags: ["TUI Apps", "irssi", "IRC"]
---

#### VIDEO

{{< amazon src="Irssi+-+Terminal+IRC+Client+-+Who+Needs+a+GUI+to+Chat!.mp4" >}}In this video I review the well-established and popular IRC client irssi.  It is a fully functional and customizable chat program that runs in your terminal.Irssi- https://irssi.org/    
