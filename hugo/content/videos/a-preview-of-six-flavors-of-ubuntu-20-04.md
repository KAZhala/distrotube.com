---
title: "A Preview Of Six Flavors Of Ubuntu 20.04 Focal Fossa"
image: images/thumbs/0604.jpg
date: 2020-04-23T12:23:40+06:00
author: Derek Taylor
tags: ["Ubuntu", "Kubuntu", "Xubuntu", "Lubuntu", "Ubuntu MATE", "Ubuntu Budgie"]
---

#### VIDEO

{{< amazon src="Six+Flavors+Of+Ubuntu+2004+Focal+Fossa.mp4" >}}
&nbsp;

#### SHOW NOTES

In this video, I take a quick look at the soon-to-be released versions of Ubuntu 20.04 codenamed "Focal Fossa."  I will briefly look at the flagship Ubuntu distribution as well as: Kubuntu, Xubuntu, Lubuntu, Ubuntu Budgie and Ubuntu MATE.

REFERENCED:
+ https://ubuntu.com/
+ https://kubuntu.org/
+ https://xubuntu.org/
+ https://lubuntu.me/
+ https://ubuntubudgie.org/
+ https://ubuntu-mate.org/
