---
title: "ArchLabs Minimo First Impression Install & Review"
image: images/thumbs/0010.jpg
date: Sat, 14 Oct 2017 01:49:31 +0000
author: Derek Taylor
tags: ["Distro Reviews", "ArchLabs", "Openbox"]
---

#### VIDEO

{{< amazon src="ArchLabs+Minimo+First+Impression+Install+%26+Review.mp4" >}}  
&nbsp;

#### SHOW NOTES

Yet another Arch-based rolling release distro. This time I take a look at ArchLabs, a Linux distro that I only recently became aware of. A minimal distro that uses the Openbox window manager. https://archlabslinux.com/
