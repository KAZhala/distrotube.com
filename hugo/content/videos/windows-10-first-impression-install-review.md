---
title: "Windows 10 First Impression Install and Review"
image: images/thumbs/0165.jpg
date: Sun, 01 Apr 2018 22:15:08 +0000
author: Derek Taylor
tags: ["Windows", ""]
---

#### VIDEO

{{< amazon src="Windows+10+First+Impression+Install+and+Review.mp4" >}}  
&nbsp;

#### SHOW NOTES

In this first impression install and review, the distro that I'm reviewing...is not a distro. It is the one and only Microsoft Windows 10. Will I be impressed or reviled by it? Would I consider switching back to living in Windows? Find out! Also, Happy Easter! 
