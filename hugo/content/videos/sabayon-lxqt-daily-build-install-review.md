---
title: "Sabayon LXQt Daily Build Install & Review"
image: images/thumbs/0063.jpg
date: Sat, 16 Dec 2017 15:21:05 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Sabayon", "LXQt"]
---

#### VIDEO

{{< amazon src="Sabayon+LXQt+Daily+Build+Install+%26+Review.mp4" >}}  
&nbsp;

#### SHOW NOTES

Doing another install of Sabayon for you guys. I needed to do this because the first video I did on Sabayon was not a good experience, mainly because I chose to install the "minimal" edition and the ISO was a year old (NOT good on a rolling release install!). This is the daily build for Sabayon's LXQt edition. <a href="https://www.sabayon.org/">https://www.sabayon.org/</a>
