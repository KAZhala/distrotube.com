---
title: "Play Video Games and Watch Movies with Telnet"
image: images/thumbs/0156.jpg
date: Sat, 24 Mar 2018 22:04:41 +0000
author: Derek Taylor
tags: ["Gaming", ""]
---

#### VIDEO

{{< amazon src="Play+Video+Games+and+Watch+Movies+with+Telnet.mp4" >}}
&nbsp;

#### SHOW NOTES

Just relaxing on a Saturday afternoon and checking out some cool games and watching a movie....in the terminal...with telnet. :D 

Some cool games: 

telnet aardmud.org 

telnet zombiemud.org 

telnet achaea.com 

telnet mud.darkerrealms.org 2000 

telnet darkrealms.ca     

Watch Star Wars in the terminal with: 

telnet towel.blinkenlights.nl
