---
title: "SwagArch GNU/Linux First Impression Install & Review"
image: images/thumbs/0062.jpg
date: Fri, 15 Dec 2017 15:19:40 +0000
author: Derek Taylor
tags: ["Distro Reviews", "SwagArch", "XFCE"]
---

#### VIDEO

{{< amazon src="SwagArch+GNULinux+First+Impression+Install+%26+Review.mp4" >}}  
&nbsp;

#### SHOW NOTES

I take a quick look at SwagArch, an Arch-based distro that uses the XFCE desktop environment and the Plank dock. Being an Arch-based distro, Swag is rolling release. Using XCFE, Swag is lightweight and fast. <a href="https://swagarch.gitlab.io/">https://swagarch.gitlab.io/</a>
