---
title: "AntiX 17 First Impression Install & Review"
image: images/thumbs/0048.jpg
date: Tue, 05 Dec 2017 14:37:46 +0000
author: Derek Taylor
tags: ["Distro Reviews", "AntiX"]
---

#### VIDEO

{{< amazon src="AntiX+17+First+Impression+Install+%26+Review.mp4" >}}  
&nbsp;

#### SHOW NOTES

I'm taking a quick look at AntiX 17, a Debian-based Linux distro that is lightweight, offers the user a choice of three different Debian branches, and comes pre-installed with a number of different window managers. <a href="https://antixlinux.com/">https://antixlinux.com/</a>
