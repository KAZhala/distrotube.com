---
title: "Is YouTube A Dying Platform?"
image: images/thumbs/0522.jpg
date: 2020-01-22T12:22:40+06:00
author: Derek Taylor
tags: ["YouTube", ""]
---

#### VIDEO

{{< amazon src="Is+YouTube+A+Dying+Platform.mp4" >}}
&nbsp;

#### SHOW NOTES

YouTube has changed a lot in the last ten years.  Where YouTube was once dominated by the independent content creator, it is now dominated by corporate channels.  YouTube has lost its soul.  Are there any good alternatives to YouTube?  

REFERENCED:
+ https://lbry.tv/@DistroTube:2