---
title: "Git Bare Repository - A Better Way To Manage Dotfiles"
image: images/thumbs/0336.jpg
date: Tue, 15 Jan 2019 20:15:06 +0000
author: Derek Taylor
tags: ["GitLab", "git"]
---

#### VIDEO

{{< amazon src="Git+Bare+Repository+A+Better+Way+To+Manager+Dotfiles.mp4" >}}
&nbsp;

#### SHOW NOTES

I've been looking for a better way to manage my dotfiles. My old method involved moving all of my dotfiles into their own directory and then symlinking each one fo them back into the directory structure. What a headache! Using git bare repositories, there is no more moving files into an initialized git repository and then creating symlinks. Now, I just add, commit and then push. Done.
