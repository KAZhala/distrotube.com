---
title: "Archman 18.05 Deepin Installation and First Look"
image: images/thumbs/0214.jpg
date: Wed, 23 May 2018 23:34:43 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Archman"]
---

#### VIDEO

{{< amazon src="Archman+18.05+Deepin+Installation+and+First+Look.mp4" >}}
&nbsp;

#### SHOW NOTES

Today, I'm taking a look at Archman 18.05 'Deepin'. This is yet another Arch-based Linux distribution that uses the Calamares installer, pamac, octopi and attempts to make Arch into a new user-friendly distro. http://archman.org/
