---
title: "Vim Macros Make The Impossible Possible"
image: images/thumbs/0421.jpg
date: Thu, 18 Jul 2019 05:40:00 +0000
author: Derek Taylor
tags: ["Vim", "terminal"]
---

#### VIDEO

{{< amazon src="Vim+Macros+Make+The+Impossible+Possible.mp4" >}}
&nbsp;

#### SHOW NOTES

A short video demonstrating the power of Vim macros.


&nbsp;
#### REFERENCED: 
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?v=0IR9WzpuWCY&amp;event=video_description&amp;redir_token=nluX2l8wW6r7C_A1Wuri3fvjcN18MTU3NTY5NzI3M0AxNTc1NjEwODcz&amp;q=https%3A%2F%2Fvim.fandom.com%2Fwiki%2FMacros" target="_blank" rel="nofollow noopener noreferrer">https://vim.fandom.com/wiki/Macros
