---
title: "Playing Overload on Linux - DT LIVE"
image: images/thumbs/0263.jpg
date: Sat, 28 Jul 2018 19:24:43 +0000
author: Derek Taylor
tags: ["Gaming", "Live Stream"]
---

#### VIDEO

{{< amazon src="Playing+Overload+on+Linux+-+DT+LIVE.mp4" >}}
&nbsp;

#### SHOW NOTES

I noticed that Steam suggests new games for me based on past games I've purchased.  So I bought the first game it suggested for me--Overload.  I paid $29.99 for it.  It's a 3D first person shooter.  Sounds fun!   And probably very difficult to maneuver.  
