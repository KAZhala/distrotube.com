---
title: "30 Day Challenge - Tiling Windows and TUI Programs"
image: images/thumbs/0173.jpg
date: Tue, 10 Apr 2018 22:25:56 +0000
author: Derek Taylor
tags: ["tiling window managers", "terminal", "command line"]
---

#### VIDEO

{{< amazon src="30+Day+Challenge+-+Tiling+Windows+and+TUI+Programs.mp4" >}}
&nbsp;

#### SHOW NOTES

For the next 30- days, I'm giving myself a challenge--live in a tiling window manager and use only terminal-based applications (where possible). What will be the result of this experiment? 
