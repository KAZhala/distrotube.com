---
title: "Microsoft Considering Buying GitHub, So I'm Moving Over to GitLab"
image: images/thumbs/0224.jpg
date: Sat, 02 Jun 2018 23:49:57 +0000
author: Derek Taylor
tags: ["Microsoft", "GitLab"]
---

#### VIDEO

{{< amazon src="Microsoft+Considering+Buying+GitHub%2C+So+Im+Moving+Over+to+GitLab.mp4" >}}
&nbsp;

#### SHOW NOTES

Microsoft and GitHub have been in talks discussing a possible acquisition of GitHub by Microsoft. What do I think? 
My new GitLab page: https://gitlab.com/dwt1
