---
title: "Video Number 500 - LIVE"
image: images/thumbs/0508.jpg
date: 2019-07-28T12:22:40+06:00
author: Derek Taylor
tags: ["Live Stream", ""]
---

#### VIDEO

{{< amazon src="Video+Number+500+-+LIVE-foKrbKZpyus.mp4" >}}
&nbsp;

#### SHOW NOTES

This live stream marks my 500th video.  Wow!  What a ride!  Join me for some reflection, some great memories, and (of course) I will interact with those of you hanging out in the YouTube chat.
