---
title: "Creating Packages For The Arch User Repository (AUR)"
image: images/thumbs/0614.jpg
date: 2020-05-05T12:23:40+06:00
author: Derek Taylor
tags: ["Arch Linux", ""]
---

#### VIDEO

{{< amazon src="Creating+Packages+For+The+Arch+User+Repository+AUR.mp4" >}}
&nbsp;

#### SHOW NOTES

So I've been playing around with packaging a few things for Arch Linux and getting those packages listed in the AUR.  In this video, I will go over the process of creating a Arch Linux package (see my previous video with Bigpod for a more detailed look at this process) and then uploading it to the AUR.

REFERENCED:
+ https://aur.archlinux.org/ - The AUR
+ https://wiki.archlinux.org/index.php/Arch_User_Repository - The Wiki page on the AUR