---
title: "A Simple File Manager Using Dmenu"
image: images/thumbs/0471.jpg
date: Thu, 28 Nov 2019 00:20:00 +0000
author: Derek Taylor
tags: ["dmenu", "shell scripting", ""]
---

#### VIDEO

{{< amazon src="A+Simple+File+Manager+Using+Dmenu.mp4" >}}
&nbsp;

#### SHOW NOTES

Dmenufm is a simple file manager using dmenu.  It is a shell script that has very few dependencies and allows you to manage your files in a suckless sorta way.  Just another example of what dmenu can do!


#### REFERENCED:
+ ► https://github.com/huijunchen9260/dmenufm
