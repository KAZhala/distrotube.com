---
title: "Obscure Window Manager Project - Awesome WM"
image: images/thumbs/0172.jpg
date: Sun, 08 Apr 2018 22:24:52 +0000
author: Derek Taylor
tags: ["awesome", "tiling window managers"]
---

#### VIDEO

{{< amazon src="Obscure+Window+Manager+Project+-+Awesome+WM.mp4" >}}
&nbsp;

#### SHOW NOTES

In this edition of the Obscure Window Manager Project, I take a quick look at Awesome WM. It is a tiling window manager configured in Lua. <a href="https://awesomewm.org/">https://awesomewm.org/</a>
